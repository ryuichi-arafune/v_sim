dnl Process this file with autoconf to produce a configure script.

dnl Every `configure' script must call `AC_INIT'
dnl $package is the name of the program
dnl $version is its number
dnl $bugreport is an email
dnl $tarname is used to call the directories, ... related to the program
AC_INIT([V_Sim], [3.9.0], [damien D caliste AT cea D fr], [v_sim])

dnl Versioning
V_SIM_MAJOR_VERSION=$(echo $PACKAGE_VERSION | cut -d'.' -f1)
V_SIM_MINOR_VERSION=$(echo $PACKAGE_VERSION | cut -d'.' -f2)
V_SIM_MICRO_VERSION=$(echo $PACKAGE_VERSION | cut -d'.' -f3)
AC_SUBST(V_SIM_MAJOR_VERSION)
AC_SUBST(V_SIM_MINOR_VERSION)
AC_SUBST(V_SIM_MICRO_VERSION)
dnl Some variables
lib_v_sim_version=$V_SIM_MINOR_VERSION":0"
AC_SUBST(lib_v_sim_version)
need_cpp_compiler="no"

dnl Save this value here, since automake will set cflags later
cflags_set=${CFLAGS}
cxxflags_set=${CXXFLAGS}

dnl Initialize automake with same $package and $version
AM_INIT_AUTOMAKE([subdir-objects])

dnl Use a config.h file to store macro definitions
AM_CONFIG_HEADER(config.h)
AC_CONFIG_MACRO_DIR([m4])

dnl Outputing some informations
date="2023-04-19"
AC_MSG_NOTICE([Compiling $PACKAGE_NAME])
AC_MSG_NOTICE([$PACKAGE_TARNAME $PACKAGE_VERSION $date])

dnl Set the default prefix
AC_PREFIX_DEFAULT([/usr/local])
AC_MSG_CHECKING([for instalation directory])
AC_MSG_RESULT([$prefix])


dnl Starting of consistency checks
dnl ------------------------------

dnl cherche un compilateur C
dnl set the CC variable
AC_PROG_CC
dnl set the CXX variable
AC_PROG_CXX
dnl set the FC variable
AC_PROG_F77
AC_PROG_FC

dnl If GCC is used, then we define the flag not to tag unused parameters.
if test "$GCC" = "yes" ; then
  AC_DEFINE([_U_], [__attribute((unused))], [Tag this parameter as unused.])
fi

dnl Using libtool to handle libraries
AC_CHECK_TOOL(LIBTOOL, libtool, :)
if test "$libtool" = ":" ; then
  AC_MSG_ERROR(["No 'libtool' program found."])
fi
AC_LIBTOOL_WIN32_DLL
AC_PROG_LIBTOOL

dnl check for platform
AC_MSG_CHECKING([for target architecture])
case x"$target" in
  xNONE | x)
    target_or_host="$host" ;;
  *)
    target_or_host="$target" ;;
esac
AC_MSG_RESULT(['$target_or_host'])
AC_MSG_CHECKING([for platform])
define_win32=0
define_X11=1
case "$target_or_host" in
  i686-w64-mingw32)
    platform=win32
    define_win32=1
    define_X11=0
    EXEEXT=.exe
    FONT_NORMAL="Sans"
    FONT_BOLD="Sans bold"
    EXTRA_LDFLAGS="-no-undefined"
    EXTRA_CFLAGS="-mms-bitfields"
    EXTRA_LIBS="-lintl"
    ;;
  *-*-cygwin*)
    platform=win32
    define_win32=1
    define_X11=0
    EXEEXT=.exe
    FONT_NORMAL="Sans"
    FONT_BOLD="Sans bold"
    EXTRA_LDFLAGS="-no-undefined"
    EXTRA_CFLAGS="-mms-bitfields -mno-cygwin"
    EXTRA_LIBS="-lintl"
    ;;
  *-mingw*)
    platform=win32
    define_win32=1
    define_X11=0
    EXEEXT=.exe
    FONT_NORMAL="Sans"
    FONT_BOLD="Sans bold"
    EXTRA_LDFLAGS="-no-undefined"
    EXTRA_CFLAGS="-mms-bitfields"
    EXTRA_LIBS="-lintl"
    ;;
  *-apple-darwin*)
    platform=Apple
    define_X11=1
    define_win32=0
    EXEEXT=
    FONT_NORMAL="Nimbus normal"
    FONT_BOLD="Nimbus bold"
    EXTRA_LDFLAGS=
    EXTRA_LIBS=
    EXTRA_CFLAGS=
    ;;
  *)
    platform=X11
    define_X11=1
    define_win32=0
    EXEEXT=
    FONT_NORMAL="Nimbus normal"
    FONT_BOLD="Nimbus bold"
    EXTRA_LDFLAGS=
    EXTRA_CFLAGS=
    ;;
esac
AC_MSG_RESULT([$platform])
AC_SUBST(FONT_NORMAL)
AC_SUBST(FONT_BOLD)
AC_SUBST(EXTRA_LDFLAGS)
AC_SUBST(EXTRA_CFLAGS)
AC_SUBST(EXTRA_LIBS)
AC_SUBST(EXEEXT)
AM_CONDITIONAL(PLATFORM_X11, test "$define_X11" = "1")
AM_CONDITIONAL(PLATFORM_WIN32, test "$define_win32" = "1")
AM_CONDITIONAL(BUILD_STATIC_BINARY, test "$define_X11" = "1")
AM_CONDITIONAL(BUILD_SHARED_BINARY, test "$define_win32" = "1")

dnl Add the languages which your application supports here.
AC_PROG_INTLTOOL([0.35.0])

GETTEXT_PACKAGE=$PACKAGE_TARNAME
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE", [Gettext package.])

AM_GLIB_GNU_GETTEXT

dnl checking for glib and gtk
if test x"$platform" = x"win32" ; then
   if test -z "$PKG_CONFIG_PATH" ; then
      export PKG_CONFIG_PATH=/usr/i686-w64-mingw32/lib/pkgconfig/
   fi
fi
PKG_CHECK_MODULES(GLIB, glib-2.0 >= 2.36.0 gio-2.0 gmodule-2.0 gobject-2.0 gthread-2.0)
PKG_CHECK_MODULES(GTKS, gtk+-3.0 >= 3.16.0 epoxy cairo >= 1.2 cairo-svg cairo-pdf gmodule-2.0)

dnl C-level YAML support.
AX_YAML()
if test x"$ax_have_yaml" != x"yes" ; then
  AC_MSG_WARN([libyaml is not available.])
else
  AC_DEFINE([HAVE_YAML], [], [If set, we can call yaml.h])
fi

dnl Customization of V_Sim
AC_MSG_CHECKING([for debug message])
with_debug_messages=no
define_debug=0
AC_ARG_ENABLE(debug-messages, AS_HELP_STRING([--enable-debug-messages], [compile V_Sim with all the debug strings, it is very verbose (default=no).]), with_debug_messages=$enableval, with_debug_messages=no)
AC_MSG_RESULT([$with_debug_messages])
if test "$with_debug_messages" = "yes" ; then
  define_debug=1
  AC_MSG_WARN([--enable-debug-messages... are you sure, this is very verbose?])
fi

dnl Building the doc
GTK_DOC_CHECK([1.3])


dnl Test for plug-ins compilation
dnl -----------------------------
dnl Nanoquanta netcdf format
AC_MSG_CHECKING([for ETSF file format support])
AC_ARG_WITH(etsf_file_format, AS_HELP_STRING([--with-etsf-file-format], [compile plug-in support for files that follow the ETSF specifications for structural positions and densities (default=no).]), [have_etsf=yes], [have_etsf=no])
AC_ARG_WITH(nanoquanta, AS_HELP_STRING([--with-nanoquanta], [deprecated, use --with-etsf-file-format instead (default=no).]), [have_nanoquanta=$withval], [have_nanoquanta=no])
if test x"$have_nanoquanta" = x"yes" ; then
  have_etsf="yes"
fi
AC_MSG_RESULT([$have_etsf])
if test x"$have_nanoquanta" = x"yes" ; then
  AC_MSG_WARN(["--with-nanoquanta is deprecated, use --with-etsf-file-format instead."])
fi
if test "$have_etsf" = "yes" ; then
  AC_CHECK_NETCDF([netcdf="yes"], [netcdf="no"], [3])
  if test "$netcdf" = "yes" ; then
    have_etsf="yes"
  else
    AC_MSG_WARN(["No 'netcdf.h' header file, libetsf.so will not be built."])
    have_etsf="no"
  fi
fi
AM_CONDITIONAL(HAVE_ETSF, test x"$have_etsf" = x"yes")

dnl OpenBabel wrapper
AC_MSG_CHECKING([for OpenBabel support])
AC_ARG_WITH(openbabel, AS_HELP_STRING([--with-openbabel], [compile plug-in support for a wrapper around the OpenBabel library (default=no).]), [have_openbabel=$withval], [have_openbabel=no])
AC_MSG_RESULT([$have_openbabel])
if test "$have_openbabel" = "yes" ; then
  PKG_CHECK_MODULES([OPENBABEL], [openbabel-3 >= 3.0.0], [ob=yes], [ob=no])
  if test "$ob" = "yes" ; then
    have_openbabel="yes"
    need_cpp_compiler="yes"
  else
    AC_MSG_WARN(["No OpenBabel lib/header file found, libobloader.so will not be built."])
    AC_MSG_WARN(["Maybe OpenBabel is not installed or version is to old (>=3.0 required)."])
    have_openbabel="no"
  fi
fi
AM_CONDITIONAL(HAVE_OPENBABEL, test x"$have_openbabel" = x"yes")

dnl XCrysDen file format
have_xsf="no"
AC_ARG_WITH(xsf, AS_HELP_STRING([--with-xsf], [compile plug-in support for files that follow the XCrysDen format for structural positions and densities (default=no).]), [have_xsf=$withval], [have_xsf=no])
AC_MSG_CHECKING([for XCrysDen file support])
AC_MSG_RESULT([$have_xsf])
AM_CONDITIONAL(HAVE_XSF, test x"$have_xsf" = x"yes")

dnl Cube file format
have_cube="yes"
AC_ARG_WITH(cube, AS_HELP_STRING([--without-cube], [compile plug-in support for Cube files (densities and structures) (default=yes).]), [have_cube=$withval], [have_cube=yes])
AC_MSG_CHECKING([for Cube file support])
AC_MSG_RESULT([$have_cube])
AM_CONDITIONAL(HAVE_CUBE, test x"$have_cube" = x"yes")

dnl ABINIT input file
ac_abinit="no"
AC_CHECK_ABINIT()
if test x"$ac_abinit_parser" = x"yes" ; then
  AC_DEFINE_UNQUOTED(AB_LIBRARY_VERSION, $AB_LIBRARY_VERSION, [ABINIT library version number.])
  AC_DEFINE_UNQUOTED(HAVE_ABINIT_PARSER,
    "$ac_abinit_parser",
    [Compile the ABINIT parser part for static linking.])
fi
AM_CONDITIONAL(HAVE_ABINIT_PARSER, test x"$ac_abinit_parser" = x"yes")
AC_SUBST(AB_CPPFLAGS)
AC_SUBST(AB_LIBS)
AM_CONDITIONAL(HAVE_ABINIT, test x"$ac_abinit" = x"yes")

dnl libmsym point group symmetry library
AC_CHECK_MSYM()
AC_SUBST(MSYM_CPPFLAGS)
AC_SUBST(MSYM_LIBS)
AM_CONDITIONAL(BUILTIN_MSYM, test x"$ac_msym" = x"builtin")
AM_CONDITIONAL(HAVE_MSYM, test x"$ac_msym" = x"yes" -o x"$ac_msym" = x"builtin")

dnl Archive input file support
ac_archives="yes"
AC_ARG_WITH([archives],
            AS_HELP_STRING([--without-archives], [add support of compression for input files (default=yes).]),
            [ac_archives=$withval], [ac_archives=yes])
if test x"$ac_archives" = x"yes" ; then
   PKG_CHECK_MODULES([LIB_ARCHIVE],
                     [libarchive >= 2.8],
                     [ac_archives=yes],
                     [ac_archives=no])
   if test x"$ac_archives" = x"yes" ; then
      AC_DEFINE([HAVE_LIB_ARCHIVE], [1], [libarchive is linkable.])
   else
      AC_MSG_WARN([libarchive is not available])
   fi
fi
AM_CONDITIONAL(HAVE_LIB_ARCHIVE, test x"$ac_archives" = x"yes")
AC_SUBST(LIB_ARCHIVE_CFLAGS)
AC_SUBST(LIB_ARCHIVE_LIBS)

dnl CrystalFp library support
ac_crystalfp="no"
AC_ARG_WITH([crystal-fp],
            AS_HELP_STRING([--with-crystal-fp], [add support crystal fingerprint calculation.]),
            [ac_crystalfp=$withval], [ac_crystalfp=no])
if test x"$ac_crystalfp" != x"no" ; then
  CRYSTALFP_CXXFLAGS="-I$ac_crystalfp"
  CRYSTALFP_LIBS="-L$ac_crystalfp -lCrystalFp"
  ac_crystalfp="yes"
fi
AM_CONDITIONAL(HAVE_CRYSTALFP, test x"$ac_crystalfp" = x"yes")
AC_SUBST(CRYSTALFP_CXXFLAGS)
AC_SUBST(CRYSTALFP_LIBS)

dnl BigDFT support
ac_bigdft="no"
AX_CHECK_BIGDFT()
AC_SUBST(BIGDFT_CPPFLAGS)
AC_SUBST(BIGDFT_LIBS)
AM_CONDITIONAL(HAVE_BIGDFT, test x"$ac_bigdft" = x"yes")

dnl Add Introspection capabilities
GOBJECT_INTROSPECTION_REQUIRED=0.9.0
AC_SUBST(GOBJECT_INTROSPECTION_REQUIRED)
PYGOBJECT_REQUIRED=2.21.0
AC_SUBST(PYGOBJECT_REQUIRED)

AC_ARG_ENABLE([introspection],
AS_HELP_STRING([--disable-introspection], [disable GObject introspection]),
[], [enable_introspection=yes])

if test "x$enable_introspection" != "xno" ; then
  dnl if test "x$ac_required_gtk" != "xgtk+-3.0 >= 2.90.2" ; then
  dnl   AC_MSG_ERROR(["GTK+ 3.0 is required for introspection."])
  dnl fi
  PKG_CHECK_MODULES([GOBJECT_INTROSPECTION],
                    [gobject-introspection-1.0 >= $GOBJECT_INTROSPECTION_REQUIRED],
                    [enable_introspection=yes],
                    [enable_introspection=no])
  PKG_CHECK_MODULES([PYGOBJECT],
                    [pygobject-3.0 >= $PYGOBJECT_REQUIRED],
                    [],
                    [enable_introspection=no])
fi
if test "x$enable_introspection" = "xyes" ; then
  girdir=$($PKG_CONFIG --variable=girdir gobject-introspection-1.0)
  AC_DEFINE([WITH_GOBJECT_INTROSPECTION], [1], [enable GObject introspection support])
  AC_SUBST(GOBJECT_INTROSPECTION_CFLAGS)
  AC_SUBST(GOBJECT_INTROSPECTION_LIBS)
  AC_SUBST([G_IR_SCANNER], [$($PKG_CONFIG --variable=g_ir_scanner gobject-introspection-1.0)])
  AC_SUBST([G_IR_COMPILER], [$($PKG_CONFIG --variable=g_ir_compiler gobject-introspection-1.0)])

  dnl Add Python support for the PythonGI plug-in.
  AM_PATH_PYTHON(3.1)
  AM_CHECK_PYTHON_HEADERS(,[AC_MSG_ERROR(could not find Python headers)])
  PYTHON_INCLUDES="`python3-config --cflags`"
  AC_SUBST(PYTHON_INCLUDES)
  PYTHON_LIBS="`python3-config --embed --ldflags`"
  AC_SUBST(PYTHON_LIBS)

  AC_MSG_CHECKING(for pygobject overrides directory)
  overrides_dir="`$PYTHON -c 'import gi; print(gi._overridesdir)' 2>/dev/null`"
  # fallback if the previous failed
  if test "x$overrides_dir" = "x" ; then
    overrides_dir="${pyexecdir}/gi/overrides"
  fi
  if ! test -w $overrides_dir ; then
    overrides_dir=${libdir}/python$PYTHON_VERSION/dist-packages/gi/overrides
  fi
  AC_MSG_RESULT($overrides_dir)
else
  overrides_dir=${libdir}/python$PYTHON_VERSION/dist-packages/gi/overrides
fi
AM_CONDITIONAL([WITH_GOBJECT_INTROSPECTION], [test "x$enable_introspection" = "xyes"])

dnl AC_PROG_CC set the CC variable and detect if we use the GNU compiler
dnl We now append some various CFLAGS depending on platform and choice
dnl of the user.
AC_ARG_WITH(strict-cflags, AS_HELP_STRING([--with-strict-cflags], [if set or absent some correctness cflags are appended to the CFLAGS variable. Appended values dependent on the platform and code branch (default on Unix for development is Wall W Werror Wpedantic).]), [STRICT_CFLAGS=$withval], [STRICT_CFLAGS="no"])
flags='Wall W'
flags_cpp='Wall W'
if test "$GCC" = "yes" -a "$STRICT_CFLAGS" = "yes"; then
  flags=$flags' Werror Wpedantic std=c99'
  flags_cpp=$flags_cpp' Werror Wpedantic'
fi
for fl in $flags ; do
  case " $CFLAGS " in
  *[\ \	]-$fl[\ \	]*) ;;
  *) CFLAGS="$CFLAGS -$fl" ;;
  esac
done
for fl in $flags_cpp ; do
  case " $CXXFLAGS " in
  *[\ \	]-$fl[\ \	]*) ;;
  *) CXXFLAGS="$CXXFLAGS -$fl" ;;
  esac
done
AC_MSG_CHECKING([for CFLAGS used])
AC_MSG_RESULT([$CFLAGS])
AC_MSG_CHECKING([for CXXFLAGS used])
AC_MSG_RESULT([$CXXFLAGS])
AC_CHECK_DECLS(strdup)

dnl compatibility for very old version of autotools
if test -z "$docdir" ; then
  docdir="$datadir/doc/$PACKAGE"
fi

dnl default installation directories
v_simexedir="$bindir"
AC_SUBST(v_simexedir)
v_simresourcesdir="$datadir/$PACKAGE"
AC_SUBST(v_simresourcesdir)
v_simpixmapsdir="$datadir/$PACKAGE/pixmaps"
AC_SUBST(v_simpixmapsdir)
v_simiconsdir="$datadir/pixmaps"
AC_SUBST(v_simiconsdir)
v_simlegaldir="${docdir}"
AC_SUBST(v_simlegaldir)
v_simexamplesdir="${docdir}/examples"
AC_SUBST(v_simexamplesdir)
v_simpluginsdir="$libdir/$PACKAGE/plug-ins"
AC_SUBST(v_simpluginsdir)
v_simgirdir="$datadir/gir-1.0"
AC_SUBST(v_simgirdir)
v_simtypelibsdir="$libdir/girepository-1.0"
AC_SUBST(v_simtypelibsdir)
v_simoverridesdir=$overrides_dir
AC_SUBST(v_simoverridesdir)

dnl Values of flags
AC_DEFINE_UNQUOTED(DEBUG, $define_debug, [Compile V_Sim in debug mode, very verbose.])
AC_DEFINE_UNQUOTED(SYSTEM_WIN32, $define_win32, [Target platform.])
AC_DEFINE_UNQUOTED(SYSTEM_X11, $define_X11, [Target platform.])
AC_DEFINE_UNQUOTED(V_SIM_RELEASE_DATE, "$date", [Date of version release.])

AC_OUTPUT([
Makefile
src/Makefile
lib/plug-ins/Makefile
lib/plug-ins/nanoquanta-netcdf/Makefile
lib/plug-ins/OpenBabel-wrapper/Makefile
lib/plug-ins/xsf/Makefile
lib/plug-ins/cube/Makefile
lib/plug-ins/abinit/Makefile
lib/plug-ins/python-gi/Makefile
lib/plug-ins/archives/Makefile
lib/plug-ins/bigdft/Makefile
lib/plug-ins/fpcrystal/Makefile
lib/plug-ins/msym/Makefile
lib/Makefile
etc/Makefile
etc/v_sim.rc
etc/v_sim.ini
pixmaps/Makefile
examples/Makefile
po/Makefile.in
Documentation/Makefile
Documentation/reference/Makefile
Documentation/reference/version
tests/Makefile
tests/core/Makefile
])

echo "
Configuration:

    Source code location:     ${srcdir}
    Destination path prefix:  ${prefix}
    Compiler:                 ${CC}
    CFLAGS:                   ${CFLAGS}
    LDFLAGS:                  ${LDFLAGS}
    GLIB-part CFLAGS:         ${GLIB_CFLAGS}
    GLIB-part LIBS:           ${GLIB_LIBS}
    GTKS-part CFLAGS:         ${GTKS_CFLAGS}
    GTKS-part LIBS:           ${GTKS_LIBS}

    Enable debug messages:    ${with_debug_messages}
    Enable gtk-doc:           ${enable_gtk_doc}
    Enable introspection:     ${enable_introspection}

    Plug-ins:
    With Nanoquanta support:  ${have_etsf}"
if test "$have_etsf" = "yes" ; then
echo \
"     | CFLAGS:                ${NC_CFLAGS}
     | LDFLAGS LIBS:          ${NC_LDFLAGS} ${NC_LIBS}"
fi
echo "    With OpenBabel support:   ${have_openbabel}"
if test "$have_openbabel" = "yes" ; then
echo \
"     | CFLAGS:                ${OPENBABEL_CFLAGS}
     | LDFLAGS LIBS:          ${OPENBABEL_LIBS}"
fi
echo "    With ABINIT support:      ${ac_abinit}"
if test "$ac_abinit" = "yes" ; then
echo \
"     | Input file support     ${ac_abinit_parser}
     | Symmetry analyser      built-in
     | CFLAGS:                ${AB_CPPFLAGS}
     | LDFLAGS LIBS:          ${AB_LIBS}"
fi
echo "    With XCrysDen support:    ${have_xsf}
    With Cube support:        ${have_cube}
    With Python scripting:    ${enable_introspection}"
if test "${enable_introspection}" = "yes" ; then
echo \
"     | CFLAGS:                ${PYTHON_INCLUDES}
     | LDFLAGS LIBS:          ${PYTHON_LIBS}"
fi
echo "    With archive support :    ${ac_archives}"
if test "${ac_archives}" = "yes" ; then
echo \
"     | CFLAGS:                ${LIB_ARCHIVE_CFLAGS}
     | LDFLAGS LIBS:          ${LIB_ARCHIVE_LIBS}"
fi
echo "    With BigDFT support :     ${ac_bigdft}"
if test "${ac_bigdft}" = "yes" ; then
echo \
"     | CFLAGS:                ${BIGDFT_CPPFLAGS}
     | LDFLAGS LIBS:          ${BIGDFT_LIBS}"
fi
echo "    With CrystalFp lib  :     ${ac_crystalfp}"
if test "${ac_crystalfp}" = "yes" ; then
echo \
"     | CXXFLAGS:              ${CRYSTALFP_CXXFLAGS}
     | LDFLAGS LIBS:          ${CRYSTALFP_LIBS}"
fi
echo "    With msym lib       :     ${ac_msym}"
if test "${ac_msym}" = "yes" ; then
echo \
"     | CPPFLAGS:              ${MSYM_CPPFLAGS}
     | LDFLAGS LIBS:          ${MSYM_LIBS}"
fi
echo \
"
    Plug-ins options:
    Need a C++ compiler:      ${need_cpp_compiler}"
if test "$need_cpp_compiler" = "yes" ; then
echo \
"     | compiler:              ${CXX}
     | CXXFLAGS:              ${CXXFLAGS}
"
fi
