#!/usr/bin/env python

from gi.repository import GLib

class Listener():
    def __init__(self, obj, signal):
        self.trigger = 0
        self.target = obj
        self.signal = obj.connect(signal, self.onTrigger)
    def __enter__(self):
        return self
    def __exit__(self, type, value, traceback):
        self.target.handler_disconnect(self.signal)
        self.target = None

    def onTrigger(self, obj, args):
        self.trigger += 1

    def triggered(self):
        ctx = GLib.MainContext.default()
        while (ctx.pending()):
            ctx.iteration(False)
        return self.trigger

    def reset(self):
        self.trigger = 0
