/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef IDPROP_H
#define IDPROP_H

#include <glib.h>
#include <glib-object.h>

#include "nodeProp.h"

G_BEGIN_DECLS

/**
 * VISU_ID_NODE_VALUES_ID:
 *
 * return the id of #VisuNodeValuesId.
 */
#define VISU_TYPE_NODE_VALUES_ID	     (visu_node_values_id_get_type ())
/**
 * VISU_NODE_VALUES_ID:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuNodeValuesId id.
 */
#define VISU_NODE_VALUES_ID(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_NODE_VALUES_ID, VisuNodeValuesId))
/**
 * VISU_NODE_VALUES_ID_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuNodeValuesIdClass.
 */
#define VISU_NODE_VALUES_ID_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_NODE_VALUES_ID, VisuNodeValuesIdClass))
/**
 * VISU_IS_NODE_VALUES_ID_ID:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the id of #VisuNodeValuesId object.
 */
#define VISU_IS_NODE_VALUES_ID(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_NODE_VALUES_ID))
/**
 * VISU_IS_NODE_VALUES_ID_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the id of #VisuNodeValuesIdClass class.
 */
#define VISU_IS_NODE_VALUES_ID_CLASS(klass) (G_TYPE_CHECK_CLASS_ID(klass, VISU_TYPE_NODE_VALUES_ID))
/**
 * VISU_NODE_VALUES_ID_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_NODE_VALUES_ID_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_NODE_VALUES_ID, VisuNodeValuesIdClass))

/**
 * VisuNodeValuesId:
 * 
 * Common name to refer to a #_VisuNodeValuesId.
 */
typedef struct _VisuNodeValuesId VisuNodeValuesId;
struct _VisuNodeValuesId
{
  VisuNodeValues parent;
};

/**
 * VisuNodeValuesIdClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuNodeValuesIdClass.
 */
typedef struct _VisuNodeValuesIdClass VisuNodeValuesIdClass;
struct _VisuNodeValuesIdClass
{
  VisuNodeValuesClass parent;
};

/**
 * visu_node_values_id_get_id:
 *
 * This method returns the id of #VisuNodeValuesId, use
 * VISU_ID_NODE_VALUES_ID instead.
 *
 * Since: 3.8
 *
 * Returns: the id of #VisuNodeValuesId.
 */
GType visu_node_values_id_get_type(void);

VisuNodeValuesId* visu_node_values_id_new(VisuNodeArray *array);

guint visu_node_values_id_getAt(const VisuNodeValuesId *vect,
                                const VisuNode *node);

G_END_DECLS

#endif
