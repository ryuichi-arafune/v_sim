/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien CALISTE,
        Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien CALISTE,
        Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef ISOLINE_H
#define ISOLINE_H

#include <glib.h>
#include <glib-object.h>

#include "plane.h"

typedef struct _VisuLine VisuLine;

#define VISU_TYPE_LINE (visu_line_get_type())
GType     visu_line_get_type(void);

VisuLine* visu_line_newFromTriangles(float **data, guint nTriangles, double isoValue);
VisuLine* visu_line_ref(VisuLine *line);
void      visu_line_unref(VisuLine *line);
void      visu_line_free(VisuLine *line);

gboolean  visu_line_addVertices(const VisuLine *line, GArray *vertices, const gfloat rgba[4]);
float*    visu_line_project(VisuLine *line, VisuPlane *plane, guint *nSeg);

double    visu_line_getValue(VisuLine *line);

#endif
