/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2018)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2018)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "poleProp.h"
#include <coreTools/toolMatrix.h>
#include <coreTools/toolPhysic.h>
#include <coreTools/atoms_yaml.h>

#include <string.h>

/**
 * SECTION:poleProp
 * @short_description: define a #VisuNodeValues object to handle
 * multipole node information.
 *
 * <para>Defines a #VisuNodeValues object to store a pole
 * information on nodes.</para>
 */

static gboolean _parse(VisuNodeValues *vals, VisuNode *node, const gchar *from);
static gchar* _serialize(const VisuNodeValues *vals, const VisuNode *node);


G_DEFINE_TYPE(VisuNodeValuesPole, visu_node_values_pole, VISU_TYPE_NODE_VALUES_FARRAY)

static void visu_node_values_pole_class_init(VisuNodeValuesPoleClass *klass)
{
  /* Connect the overloading methods. */
  VISU_NODE_VALUES_CLASS(klass)->parse = _parse;
  VISU_NODE_VALUES_CLASS(klass)->serialize = _serialize;
}

static void visu_node_values_pole_init(VisuNodeValuesPole *self _U_)
{
}

static gboolean _parse(VisuNodeValues *vals, VisuNode *node, const gchar *from)
{
  gfloat data[9];
  GValue value = G_VALUE_INIT;
  PosinpDict *dict;
  guint i;

  g_return_val_if_fail(from, FALSE);

  dict = posinp_yaml_parse_properties(from, NULL);
  if (!dict)
    return FALSE;
  memset(vals, '\0', sizeof(gfloat) * 9);
  for (i = 0; i < dict->len && dict->items[i].key; i++)
    if (!g_strcmp0(dict->items[i].key, "q0") &&
        dict->items[i].type == POSINP_TYPE_DBL_ARR &&
        dict->items[i].value.darr.len == 1)
      data[0] = dict->items[i].value.darr.arr[0];
    else if (!g_strcmp0(dict->items[i].key, "q1") &&
             dict->items[i].type == POSINP_TYPE_DBL_ARR &&
             dict->items[i].value.darr.len == 3)
      {
        data[1] = dict->items[i].value.darr.arr[0];
        data[2] = dict->items[i].value.darr.arr[1];
        data[3] = dict->items[i].value.darr.arr[2];
      }
    else if (!g_strcmp0(dict->items[i].key, "q2") &&
             dict->items[i].type == POSINP_TYPE_DBL_ARR &&
             dict->items[i].value.darr.len == 5)
      {
        data[4] = dict->items[i].value.darr.arr[0];
        data[5] = dict->items[i].value.darr.arr[1];
        data[6] = dict->items[i].value.darr.arr[2];
        data[7] = dict->items[i].value.darr.arr[3];
        data[8] = dict->items[i].value.darr.arr[4];
      }
  posinp_yaml_free_properties(dict);
  g_value_set_pointer(&value, data);
  return visu_node_values_setAt(vals, node, &value);
}
static gchar* _serialize(const VisuNodeValues *vals, const VisuNode *node)
{
  GValue value = G_VALUE_INIT;
  gfloat *fvals;

  if (!visu_node_values_getAt(vals, node, &value))
    return (gchar*)0;

  fvals = (float*)g_value_get_pointer(&value);
  if (fvals)
    return g_strdup_printf("q0: [%#.3g]\nq1: [%#.3g, %#.3g, %#.3g]\nq2: [%#.3g, %#.3g, %#.3g, %#.3g, %#.3g]", fvals[0], fvals[1], fvals[2], fvals[3], fvals[4], fvals[5], fvals[6], fvals[7], fvals[8]);
  else
    return (gchar*)0;
}

/**
 * visu_node_values_pole_new:
 * @arr: a #VisuNodeArray object.
 * @label: a translatable label.
 *
 * Create a new pole field located on nodes.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly created #VisuNodeValuesPole object.
 **/
VisuNodeValuesPole* visu_node_values_pole_new(VisuNodeArray *arr,
                                                  const gchar *label)
{
  VisuNodeValuesPole *vals;

  vals = VISU_NODE_VALUES_POLE(g_object_new(VISU_TYPE_NODE_VALUES_POLE,
                                            "nodes", arr, "label", label,
                                            "type", G_TYPE_FLOAT,
                                            "n-elements", 9, NULL));
  return vals;
}

static const gfloat zmono[1] = {0.f};
static const gfloat zdi[3]   = {0.f, 0.f, 0.f};
static const gfloat zquad[5] = {0.f, 0.f, 0.f, 0.f, 0.f};
/**
 * visu_node_values_pole_getAt:
 * @pole: a #VisuNodeValuesPole object.
 * @node: a #VisuNode object.
 * @order: a #VisuPoleOrder value.
 *
 * Retrieves the pole hosted on @node.
 *
 * Since: 3.8
 *
 * Returns: (array fixed-size=3) (transfer none): the coordinates of
 * pole for @node.
 **/
const gfloat* visu_node_values_pole_getAt(const VisuNodeValuesPole *pole,
                                          const VisuNode *node,
                                          VisuPoleOrder order)
{
  GValue diffValue = G_VALUE_INIT;
  const gfloat *diff;

  diff = (const gfloat*)0;
  if (visu_node_values_getAt(VISU_NODE_VALUES(pole), node, &diffValue))
    diff = (const gfloat*)g_value_get_pointer(&diffValue);
  switch (order)
    {
    case VISU_MONOPOLE:
      return (diff) ? diff : zmono;
    case VISU_DIPOLE:
      return (diff) ? diff + 1: zdi;
    case VISU_QUADRUPOLE:
      return (diff) ? diff + 4: zquad;
    default:
      return (const gfloat*)0;
    }
}

/**
 * visu_node_values_pole_setMonoAt:
 * @pole: a #VisuNodeValuesPole object.
 * @node: a #VisuNode object.
 * @val: a mono-pole value.
 *
 * Changes the mono-pole hosted at @node.
 *
 * Since: 3.8
 *
 * Returns: TRUE if pole for @node is indeed changed.
 **/
gboolean visu_node_values_pole_setMonoAt(VisuNodeValuesPole *pole,
                                         const VisuNode *node, gfloat val)
{
  gfloat *vals, poles[9];
  GValue gval = G_VALUE_INIT;

  visu_node_values_getAt(VISU_NODE_VALUES(pole), node, &gval);
  vals = (gfloat*)g_value_get_pointer(&gval);
  if (vals && vals[0] == val)
    return FALSE;

  if (!vals)
    {
      vals = poles;
      memset(poles, '\0', sizeof(gfloat) * 9);
    }
  vals[0] = val;
  g_value_set_pointer(&gval, vals);
  return visu_node_values_setAt(VISU_NODE_VALUES(pole), node, &gval);
}
/**
 * visu_node_values_pole_setMonoAtDbl:
 * @pole: a #VisuNodeValuesPole object.
 * @node: a #VisuNode object.
 * @val: a mono-pole value.
 *
 * Changes the mono-pole hosted at @node.
 *
 * Since: 3.8
 *
 * Returns: TRUE if pole for @node is indeed changed.
 **/
gboolean visu_node_values_pole_setMonoAtDbl(VisuNodeValuesPole *pole,
                                            const VisuNode *node, gdouble val)
{
  return visu_node_values_pole_setMonoAt(pole, node, val);
}

/**
 * visu_node_values_pole_setDiAt:
 * @pole: a #VisuNodeValuesPole object.
 * @node: a #VisuNode object.
 * @val: a di-pole value.
 *
 * Changes the di-pole hosted at @node.
 *
 * Since: 3.8
 *
 * Returns: TRUE if pole for @node is indeed changed.
 **/
gboolean visu_node_values_pole_setDiAt(VisuNodeValuesPole *pole,
                                       const VisuNode *node, const gfloat val[3])
{
  gfloat *vals, poles[9];
  GValue gval = G_VALUE_INIT;

  visu_node_values_getAt(VISU_NODE_VALUES(pole), node, &gval);
  vals = (gfloat*)g_value_get_pointer(&gval);
  if (vals && vals[1] == val[0] && vals[2] == val[1] && vals[3] == val[2])
    return FALSE;

  if (!vals)
    {
      vals = poles;
      memset(poles, '\0', sizeof(gfloat) * 9);
    }
  vals[1] = val[0];
  vals[2] = val[1];
  vals[3] = val[2];
  g_value_set_pointer(&gval, vals);
  return visu_node_values_setAt(VISU_NODE_VALUES(pole), node, &gval);
}
/**
 * visu_node_values_pole_setDiAtDbl:
 * @pole: a #VisuNodeValuesPole object.
 * @node: a #VisuNode object.
 * @val: a di-pole value.
 *
 * Changes the di-pole hosted at @node.
 *
 * Since: 3.8
 *
 * Returns: TRUE if pole for @node is indeed changed.
 **/
gboolean visu_node_values_pole_setDiAtDbl(VisuNodeValuesPole *pole,
                                          const VisuNode *node, const gdouble val[3])
{
  gfloat fval[3];
  fval[0] = val[0];
  fval[1] = val[1];
  fval[2] = val[2];
  return visu_node_values_pole_setDiAt(pole, node, fval);
}

/**
 * visu_node_values_pole_setQuadAt:
 * @pole: a #VisuNodeValuesPole object.
 * @node: a #VisuNode object.
 * @val: a quadru-pole value.
 *
 * Changes the quadru-pole hosted at @node.
 *
 * Since: 3.8
 *
 * Returns: TRUE if pole for @node is indeed changed.
 **/
gboolean visu_node_values_pole_setQuadAt(VisuNodeValuesPole *pole,
                                         const VisuNode *node, const gfloat val[5])
{
  gfloat *vals, poles[9];
  GValue gval = G_VALUE_INIT;

  visu_node_values_getAt(VISU_NODE_VALUES(pole), node, &gval);
  vals = (gfloat*)g_value_get_pointer(&gval);
  if (vals && vals[4] == val[0] && vals[5] == val[1] && vals[6] == val[2] && vals[7] == val[3] && vals[8] == val[4])
    return FALSE;

  if (!vals)
    {
      vals = poles;
      memset(poles, '\0', sizeof(gfloat) * 9);
    }
  vals[4] = val[0];
  vals[5] = val[1];
  vals[6] = val[2];
  vals[7] = val[3];
  vals[8] = val[4];
  g_value_set_pointer(&gval, vals);
  return visu_node_values_setAt(VISU_NODE_VALUES(pole), node, &gval);
}
/**
 * visu_node_values_pole_setQuadAtDbl:
 * @pole: a #VisuNodeValuesPole object.
 * @node: a #VisuNode object.
 * @val: a quadru-pole value.
 *
 * Changes the quadru-pole hosted at @node.
 *
 * Since: 3.8
 *
 * Returns: TRUE if pole for @node is indeed changed.
 **/
gboolean visu_node_values_pole_setQuadAtDbl(VisuNodeValuesPole *pole,
                                            const VisuNode *node, const gdouble val[5])
{
  gfloat fval[5];
  fval[0] = val[0];
  fval[1] = val[1];
  fval[2] = val[2];
  fval[3] = val[3];
  fval[4] = val[4];
  return visu_node_values_pole_setQuadAt(pole, node, fval);
}
