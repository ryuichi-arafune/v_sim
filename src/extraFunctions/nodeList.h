/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2019)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2019)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef NODELIST_H
#define NODELIST_H

#include <visu_tools.h>
#include <visu_nodes.h>
#include "fragProp.h"
#include "sfielddata.h"

G_BEGIN_DECLS

#define VISU_TYPE_NODE_LIST	          (visu_node_list_get_type ())
#define VISU_NODE_LIST(obj)	          (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_NODE_LIST, VisuNodeList))
#define VISU_NODE_LIST_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_NODE_LIST, VisuNodeListClass))
#define VISU_IS_NODE_LIST(obj)         (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_NODE_LIST))
#define VISU_IS_NODE_LIST_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_NODE_LIST))
#define VISU_NODE_LIST_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_NODE_LIST, VisuNodeListClass))

typedef struct _VisuNodeList        VisuNodeList;
typedef struct _VisuNodeListPrivate VisuNodeListPrivate;
typedef struct _VisuNodeListClass   VisuNodeListClass;

/**
 * VisuNodeList:
 *
 * An opaque structure.
 *
 * Since: 3.8
 */
struct _VisuNodeList
{
  VisuObject parent;

  VisuNodeListPrivate *priv;
};

/**
 * VisuNodeListClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuNodeListClass structure.
 *
 * Since: 3.8
 */
struct _VisuNodeListClass
{
  VisuObjectClass parent;
};

/**
 * visu_node_list_get_type:
 *
 * This method returns the type of #VisuNodeList, use
 * VISU_TYPE_NODE_LIST instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuNodeList.
 */
GType visu_node_list_get_type(void);

VisuNodeList* visu_node_list_new(VisuNodeArray *array);
VisuNodeList* visu_node_list_new_fromFrag(VisuNodeValuesFrag *frag,
                                          const gchar *label);

gboolean visu_node_list_add(VisuNodeList *list, guint id);
gboolean visu_node_list_remove(VisuNodeList *list, guint id);

VisuScalarFieldData* visu_node_list_envelope(VisuNodeList *list);

G_END_DECLS

#endif
