/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2013)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2013)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "maps.h"

#include <epoxy/gl.h>

#include <extraFunctions/isoline.h>

/**
 * SECTION:maps
 * @short_description: Defines methods to draw maps.
 *
 * <para>Maps are coloured representation of a #VisuScalarField on a #VisuPlane.</para>
 */

/**
 * VisuGlExtMapsClass:
 * @parent: the parent class;
 * @added: a method that is called when a #VisuMap is added to a
 * #VisuGlExtMaps object.
 * @removed: a method that is called when a #VisuMap is removed from a
 * #VisuGlExtMaps object.
 *
 * A short way to identify #_VisuGlExtMapsClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtMaps:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtMapsPrivate:
 *
 * Private fields for #VisuGlExtMaps objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtMapsPrivate
{
  gboolean dispose_has_run;

  GList *maps;

  /* Objects for rendering. */
  VisuGlView *view;
  gulong gross_signal, detail_signal;
};

typedef struct _MapHandleStruct
{
  VisuGlExtMaps *maps;
  VisuMap *map;
  gulong chg_signal;
  gboolean isBuilt;
  float prec;
  ToolShade *shade;
  ToolColor *color;
  gboolean alpha;
} _MapHandle;

enum
  {
    BUF_LINES,
    BUF_FRAME,
    BUF_MAP,
    N_BUFFERS
  };
enum
  {
    ADDED_SIGNAL,
    REMOVED_SIGNAL,
    N_SIGNALS
  };
static guint _signals[N_SIGNALS] = { 0 };

static GObject* visu_gl_ext_maps_constructor(GType gtype, guint nprops, GObjectConstructParam *props);
static void visu_gl_ext_maps_finalize(GObject* obj);
static void visu_gl_ext_maps_dispose(GObject* obj);
static void visu_gl_ext_maps_rebuild(VisuGlExt *ext);
static void visu_gl_ext_maps_draw(VisuGlExt *maps);
static void visu_gl_ext_maps_render(const VisuGlExt *maps);
static gboolean visu_gl_ext_maps_setGlView(VisuGlExt *maps, VisuGlView *view);
static gboolean _add(VisuGlExtMaps *maps, VisuMap *map,
                     float prec, ToolShade *shade,
                     const ToolColor *color, gboolean alpha);

/* Local callbacks */
static void onViewChange(VisuGlView *view, gpointer data);
static void onMapChange(VisuMap *map, gpointer data);

/* Local routines. */
static void _freeMapHandle(gpointer obj)
{
  _MapHandle *mhd;

  mhd = (_MapHandle*)obj;
  if (VISU_GL_EXT_MAPS_GET_CLASS(mhd->maps)->removed)
    VISU_GL_EXT_MAPS_GET_CLASS(mhd->maps)->removed(mhd->maps, mhd->map);
  g_debug("Extension Maps: emiting %p map removed.", (gpointer)mhd->map);
  g_signal_emit(G_OBJECT(mhd->maps), _signals[REMOVED_SIGNAL], 0, mhd->map);
  g_debug("Extension Maps: emission done.");
  g_signal_handler_disconnect(G_OBJECT(mhd->map), mhd->chg_signal);
  g_object_unref(G_OBJECT(mhd->map));
  tool_shade_free(mhd->shade);
  g_free(mhd->color);
  g_slice_free1(sizeof(_MapHandle), obj);
}
static gpointer _newMapHandle(VisuGlExtMaps *maps, VisuMap *map,
                              float prec, ToolShade *shade, const ToolColor *color, gboolean alpha)
{
  _MapHandle *mhd;

  g_object_ref(G_OBJECT(map));
  mhd = g_slice_alloc(sizeof(_MapHandle));
  g_debug("Extension Maps: add listeners on map %p.", (gpointer)map);
  mhd->maps = maps;
  mhd->map = map;
  mhd->chg_signal = g_signal_connect(G_OBJECT(map), "changed",
                                     G_CALLBACK(onMapChange), maps);
  mhd->isBuilt = FALSE;
  mhd->prec = prec;
  mhd->shade = tool_shade_copy(shade);
  mhd->color = (color) ? g_boxed_copy(TOOL_TYPE_COLOR, color) : (ToolColor*)0;
  mhd->alpha = alpha;
  if (maps->priv->view)
    visu_map_setLevel(mhd->map,
                      visu_gl_view_getPrecision(maps->priv->view),
                      maps->priv->view->camera.gross,
                      tool_gl_camera_getRefLength(&maps->priv->view->camera, (ToolUnits*)0));
  if (VISU_GL_EXT_MAPS_GET_CLASS(maps)->added)
    VISU_GL_EXT_MAPS_GET_CLASS(maps)->added(maps, map);
  return (gpointer)mhd;
}
static gint _cmpMapHandle(gconstpointer a, gconstpointer b)
{
  _MapHandle *mhd_a = (_MapHandle*)a;
  
  if (mhd_a->map == b)
    return 0;
  return 1;
}
#define _getMap(H) ((_MapHandle*)H)->map

G_DEFINE_TYPE_WITH_CODE(VisuGlExtMaps, visu_gl_ext_maps, VISU_TYPE_GL_EXT,
                        G_ADD_PRIVATE(VisuGlExtMaps))

static void visu_gl_ext_maps_class_init(VisuGlExtMapsClass *klass)
{
  g_debug("Extension Maps: creating the class of the object.");
  /* g_debug("                - adding new signals ;"); */

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->constructor = visu_gl_ext_maps_constructor;
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_maps_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_maps_finalize;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_maps_rebuild;
  VISU_GL_EXT_CLASS(klass)->draw = visu_gl_ext_maps_draw;
  VISU_GL_EXT_CLASS(klass)->render = visu_gl_ext_maps_render;
  VISU_GL_EXT_CLASS(klass)->setGlView = visu_gl_ext_maps_setGlView;
  VISU_GL_EXT_MAPS_CLASS(klass)->add = _add;

  /**
   * VisuGlExtMaps::added:
   * @maps: the object emitting the signal.
   * @map: the added #VisuMap.
   *
   * This signal is emitted when @map is added to @maps.
   *
   * Since: 3.8
   */
  _signals[ADDED_SIGNAL] =
    g_signal_new("added", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 G_STRUCT_OFFSET(VisuGlExtMapsClass, added), NULL, NULL,
                 g_cclosure_marshal_VOID__OBJECT, G_TYPE_NONE, 1, VISU_TYPE_MAP);
  /**
   * VisuGlExtMaps::removed:
   * @maps: the object emitting the signal.
   * @map: the removed #VisuMap.
   *
   * This signal is emitted when @map is removed to @maps.
   *
   * Since: 3.8
   */
  _signals[REMOVED_SIGNAL] =
    g_signal_new("removed", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 G_STRUCT_OFFSET(VisuGlExtMapsClass, removed), NULL, NULL,
                 g_cclosure_marshal_VOID__OBJECT, G_TYPE_NONE, 1, VISU_TYPE_MAP);
}

static GObject* visu_gl_ext_maps_constructor(GType gtype, guint nprops, GObjectConstructParam *props)
{
  guint i;

  for (i = 0; i < nprops; ++i)
    {
      if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "nGlObj"))
        g_value_set_uint(props[i].value, N_BUFFERS);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "nGlProg"))
        g_value_set_uint(props[i].value, 1);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "withFog"))
        g_value_set_uint(props[i].value, TRUE);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "priority"))
        g_value_set_uint(props[i].value, VISU_GL_EXT_PRIORITY_NORMAL - 1);
    }

  return G_OBJECT_CLASS(visu_gl_ext_maps_parent_class)->constructor(gtype, nprops, props);
}
static void visu_gl_ext_maps_init(VisuGlExtMaps *obj)
{
  g_debug("Extension Maps: initializing a new object (%p).",
	      (gpointer)obj);
  
  obj->priv = visu_gl_ext_maps_get_instance_private(obj);
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->maps            = (GList*)0;
  obj->priv->view            = (VisuGlView*)0;
  obj->priv->gross_signal    = 0;
  obj->priv->detail_signal   = 0;
}
static void visu_gl_ext_maps_dispose(GObject* obj)
{
  VisuGlExtMaps *maps;
  GList *lst;

  g_debug("Extension Maps: dispose object %p.", (gpointer)obj);

  maps = VISU_GL_EXT_MAPS(obj);
  if (maps->priv->dispose_has_run)
    return;
  maps->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_maps_setGlView(VISU_GL_EXT(obj), (VisuGlView*)0);
  g_debug("Extension MapSet: clearing a list of %d maps.",
              g_list_length(maps->priv->maps));
  for (lst = maps->priv->maps; lst; lst = g_list_next(lst))
    _freeMapHandle(lst->data);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_maps_parent_class)->dispose(obj);
}
static void visu_gl_ext_maps_finalize(GObject* obj)
{
  VisuGlExtMaps *maps = VISU_GL_EXT_MAPS(obj);

  g_return_if_fail(obj);

  g_debug("Extension Maps: finalize object %p.", (gpointer)obj);
  /* Free privs elements. */
  if (maps->priv)
    g_list_free(maps->priv->maps);

  /* Chain up to the parent class */
  g_debug("Extension Maps: chain to parent.");
  G_OBJECT_CLASS(visu_gl_ext_maps_parent_class)->finalize(obj);
  g_debug("Extension Maps: freeing ... OK.");
}

/**
 * visu_gl_ext_maps_new:
 * @name: (allow-none): the name to give to the extension (default is #VISU_GL_EXT_MAPS_ID).
 *
 * Creates a new #VisuGlExt to draw maps.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtMaps* visu_gl_ext_maps_new(const gchar *name)
{
  g_debug("Extension Maps: new object.");
  
  return g_object_new(VISU_TYPE_GL_EXT_MAPS,
                      "name", name ? name : VISU_GL_EXT_MAPS_ID, "label", _(name),
                      "description", _("Drawing extension for maps."), NULL);
}

static void _setZoomLevel(VisuGlExtMaps *maps)
{
  GList *lst;
  _MapHandle *mhd;

  /* Adjust zoom level for all maps. */
  for (lst = maps->priv->maps; lst; lst = g_list_next(lst))
    {
      mhd = (_MapHandle*)lst->data;
      visu_map_setLevel(mhd->map,
                        visu_gl_view_getPrecision(maps->priv->view),
                        maps->priv->view->camera.gross,
                        tool_gl_camera_getRefLength(&maps->priv->view->camera, (ToolUnits*)0));
      mhd->isBuilt = FALSE;
    }

  visu_gl_ext_setDirty(VISU_GL_EXT(maps), VISU_GL_DRAW_REQUIRED);
}
static gboolean visu_gl_ext_maps_setGlView(VisuGlExt *maps, VisuGlView *view)
{
  VisuGlExtMapsPrivate *priv = VISU_GL_EXT_MAPS(maps)->priv;

  /* No change to be done. */
  if (view == priv->view)
    return FALSE;

  if (priv->view)
    {
      g_signal_handler_disconnect(G_OBJECT(priv->view), priv->detail_signal);
      g_object_unref(priv->view);
    }
  priv->view = view;
  if (view)
    {
      g_object_ref(view);
      priv->detail_signal =
        g_signal_connect(G_OBJECT(view), "DetailLevelChanged",
                         G_CALLBACK(onViewChange), (gpointer)maps);
      _setZoomLevel(VISU_GL_EXT_MAPS(maps));
    }
  else
    {
      /* priv->gross_signal     = 0; */
      priv->detail_signal     = 0;
    }

  return TRUE;
}

/**
 * visu_gl_ext_maps_add:
 * @maps: a #VisuGlExtMaps object.
 * @map: (transfer full): a #VisuMaps object.
 * @prec: rendering adaptivity level (default is 100).
 * @shade: (transfer full): a #ToolShade object.
 * @color: (transfer full) (allow-none): a #ToolColor object.
 * @alpha: a boolean.
 *
 * Add a new map to the list of drawn maps. If @color is %NULL, then
 * iso-lines will be drawn in inverse color. If @alpha is TRUE, the
 * map will be rendered with alpha blending when values go to zero.
 *
 * Since: 3.7
 *
 * Returns: FALSE if @surf was already reguistered.
 **/
gboolean visu_gl_ext_maps_add(VisuGlExtMaps *maps, VisuMap *map,
                              float prec, ToolShade *shade,
                              const ToolColor *color, gboolean alpha)
{
  VisuGlExtMapsClass *klass = VISU_GL_EXT_MAPS_GET_CLASS(maps);
  g_return_val_if_fail(klass && klass->add, FALSE);

  return klass->add(maps, map, prec, shade, color, alpha);
}
static gboolean _add(VisuGlExtMaps *maps, VisuMap *map,
                     float prec, ToolShade *shade,
                     const ToolColor *color, gboolean alpha)
{
  GList *lst;

  g_return_val_if_fail(VISU_IS_GL_EXT_MAPS(maps), FALSE);

  lst = g_list_find_custom(maps->priv->maps, map, _cmpMapHandle);
  if (lst)
    return FALSE;

  maps->priv->maps = g_list_prepend(maps->priv->maps,
                                    _newMapHandle(maps, map, prec, shade, color, alpha));
  g_debug("Extension MapSet: adding map %p (%d).",
              (gpointer)map, g_list_length(maps->priv->maps));
  g_signal_emit(G_OBJECT(maps), _signals[ADDED_SIGNAL], 0, map);

  visu_gl_ext_setDirty(VISU_GL_EXT(maps), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
/**
 * visu_gl_ext_maps_remove:
 * @maps: a #VisuGlExtMaps object.
 * @map: a #VisuMaps object.
 *
 * Removes @map from the list of drawn maps.
 *
 * Since: 3.7
 *
 * Returns: TRUE if @map was part of drawn maps.
 **/
gboolean visu_gl_ext_maps_remove(VisuGlExtMaps *maps, VisuMap *map)
{
  GList *lst;

  g_return_val_if_fail(VISU_IS_GL_EXT_MAPS(maps), FALSE);
  
  lst = g_list_find_custom(maps->priv->maps, map, _cmpMapHandle);
  if (!lst)
    return FALSE;

  maps->priv->maps = g_list_remove_link(maps->priv->maps, lst);
  _freeMapHandle(lst->data);
  g_list_free(lst);
  g_debug("Extension MapSet: removing map %p (%d).",
              (gpointer)map, g_list_length(maps->priv->maps));

  visu_gl_ext_setDirty(VISU_GL_EXT(maps), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
/**
 * visu_gl_ext_maps_removeAll:
 * @maps: a #VisuGlExtMaps object.
 *
 * Removes all the #VisuMap stored in @maps.
 *
 * Since: 3.8
 **/
void visu_gl_ext_maps_removeAll(VisuGlExtMaps *maps)
{
  g_return_if_fail(VISU_IS_GL_EXT_MAPS(maps));
  
  g_list_free_full(maps->priv->maps, _freeMapHandle);
  maps->priv->maps = (GList*)0;
  g_debug("Extension MapSet: removing all.");
  visu_gl_ext_setDirty(VISU_GL_EXT(maps), VISU_GL_DRAW_REQUIRED);
}

static gboolean _getMapIter(VisuGlExtMaps *maps, VisuMap *map, GList *iter)
{
  GList *lst;

  if (map)
    {
      lst = g_list_find_custom(maps->priv->maps, map, _cmpMapHandle);
      if (!lst)
        return FALSE;
      iter->data = lst->data;
      iter->next = (GList*)0;
    }
  else
    {
      if (!maps->priv->maps)
        return FALSE;
      *iter = *maps->priv->maps;
    }
  return TRUE;
}
/**
 * visu_gl_ext_maps_setPrecision:
 * @maps: a #VisuGlExtMaps object.
 * @map: a #VisuMap object.
 * @prec: a floating point value (default is 100).
 *
 * Changes the adaptative mesh of @map. At a value of 200, there is no
 * adaptivity and all triangles are rendered. At a level of 100, a
 * variation of less than 3% on neighbouring triangles make them merged.
 *
 * Since: 3.7
 *
 * Returns: TRUE if @prec of @map is changed.
 **/
gboolean visu_gl_ext_maps_setPrecision(VisuGlExtMaps *maps, VisuMap *map, float prec)
{
  GList *lst, iter;
  _MapHandle *mhd;
  gboolean diff;

  g_return_val_if_fail(VISU_IS_GL_EXT_MAPS(maps), FALSE);

  if (!_getMapIter(maps, map, &iter))
    return FALSE;
  
  diff = FALSE;
  for (lst = &iter; lst; lst = g_list_next(lst))
    {
      mhd = (_MapHandle*)lst->data;
      if (mhd->prec != prec)
        {
          mhd->prec = prec;
          mhd->isBuilt = FALSE;
          diff = TRUE;
        }
    }
  if (diff)
    visu_gl_ext_setDirty(VISU_GL_EXT(maps), VISU_GL_DRAW_REQUIRED);
  return diff;
}
/**
 * visu_gl_ext_maps_setShade:
 * @maps: a #VisuGlExtMaps object.
 * @map: a #VisuMap object.
 * @shade: (allow-none) (transfer full): a #ToolShade object.
 *
 * Changes the #ToolShade used to render data variation on the @map.
 *
 * Since: 3.7
 *
 * Returns: TRUE if @shade of @map is changed.
 **/
gboolean visu_gl_ext_maps_setShade(VisuGlExtMaps *maps, VisuMap *map, ToolShade *shade)
{
  GList *lst, iter;
  _MapHandle *mhd;
  gboolean diff;

  g_return_val_if_fail(VISU_IS_GL_EXT_MAPS(maps), FALSE);

  g_debug("Extension Maps: change shade (for maps %p).", (gpointer)map);
  if (!_getMapIter(maps, map, &iter))
    return FALSE;
  
  diff = FALSE;
  for (lst = &iter; lst; lst = g_list_next(lst))
    {
      mhd = (_MapHandle*)lst->data;
      g_debug(" | map %p, update %d.", (gpointer)mhd->map,
                  !tool_shade_compare(mhd->shade, shade));
      if (!tool_shade_compare(mhd->shade, shade))
        {
          tool_shade_free(mhd->shade);
          mhd->shade = tool_shade_copy(shade);
          mhd->isBuilt = FALSE;
          diff = TRUE;
        }
    }
  if (diff)
    visu_gl_ext_setDirty(VISU_GL_EXT(maps), VISU_GL_DRAW_REQUIRED);
  return diff;
}
/**
 * visu_gl_ext_maps_setLineColor:
 * @maps: a #VisuGlExtMaps object.
 * @map: a #VisuMap object.
 * @color: (allow-none) (transfer full): a #ToolColor object.
 *
 * Changes the rendered isoline color of @map to @color. If @color is
 * %NULL, then the isolines will be color inversed to the #ToolShade
 * of @map (see visu_gl_ext_maps_setShade()).
 *
 * Since: 3.7
 *
 * Returns: TRUE if @color of @map is changed.
 **/
gboolean visu_gl_ext_maps_setLineColor(VisuGlExtMaps *maps, VisuMap *map,
                                       const ToolColor *color)
{
  GList *lst, iter;
  _MapHandle *mhd;
  gboolean diff;

  g_return_val_if_fail(VISU_IS_GL_EXT_MAPS(maps), FALSE);

  if (!_getMapIter(maps, map, &iter))
    return FALSE;
  
  if (color)
  diff = FALSE;
  for (lst = &iter; lst; lst = g_list_next(lst))
    {
      mhd = (_MapHandle*)lst->data;
      if (!tool_color_equal(mhd->color, color))
        {
          g_free(mhd->color);
          mhd->color = (color) ? g_boxed_copy(TOOL_TYPE_COLOR, color) : (ToolColor*)0;
          mhd->isBuilt = FALSE;
          diff = TRUE;
        }
    }
  if (diff)
    visu_gl_ext_setDirty(VISU_GL_EXT(maps), VISU_GL_DRAW_REQUIRED);
  return diff;
}
/**
 * visu_gl_ext_maps_setTransparent:
 * @maps: a #VisuGlExtMaps object.
 * @map: a #VisuMap object.
 * @alpha: a boolean.
 *
 * Sets if @map is rendered with transparency or not. If @alpha is
 * %TRUE, the lower the rendered value is, the more transparent the
 * colour will be.
 *
 * Since: 3.7
 *
 * Returns: TRUE if transparency of @map is changed.
 **/
gboolean visu_gl_ext_maps_setTransparent(VisuGlExtMaps *maps, VisuMap *map, gboolean alpha)
{
  GList *lst, iter;
  _MapHandle *mhd;
  gboolean diff;

  g_return_val_if_fail(VISU_IS_GL_EXT_MAPS(maps), FALSE);

  if (!_getMapIter(maps, map, &iter))
    return FALSE;
  
  diff = FALSE;
  for (lst = &iter; lst; lst = g_list_next(lst))
    {
      mhd = (_MapHandle*)lst->data;
      if (mhd->alpha != alpha)
        {
          mhd->alpha = alpha;
          mhd->isBuilt = FALSE;
          diff = TRUE;
        }
    }
  if (diff)
    visu_gl_ext_setDirty(VISU_GL_EXT(maps), VISU_GL_DRAW_REQUIRED);
  return diff;
}

static void visu_gl_ext_maps_draw(VisuGlExt *maps)
{
  _MapHandle *mhd;
  GList *lst;
  #if DEBUG == 1
  GTimer *timer;
  gulong fractionTimer;
  #endif
  VisuGlExtMapsPrivate *priv = VISU_GL_EXT_MAPS(maps)->priv;
  GArray *bufLines, *bufFrame, *bufMap;

  g_debug("Extension Maps: rebuilding map list.");
  visu_gl_ext_clearBuffers(maps);

  if (!priv->view || !priv->maps)
    return;

#if DEBUG == 1
  timer = g_timer_new();
  g_timer_start(timer);
#endif

  bufLines = visu_gl_ext_startBuffer(maps, 0, BUF_LINES, VISU_GL_RGBA_XYZ, VISU_GL_LINES);
  bufFrame = visu_gl_ext_startBuffer(maps, 0, BUF_FRAME, VISU_GL_RGBA_XYZ, VISU_GL_LINE_LOOP);
  bufMap = visu_gl_ext_startBuffer(maps, 0, BUF_MAP, VISU_GL_RGBA_XYZ, VISU_GL_TRIANGLES);
  
  /* Rebuild maps if required. */
  for (lst = priv->maps; lst; lst = g_list_next(lst))
    {
      GArray *lines;
      VisuPlane *plane;
      guint i;
      const GLfloat black[4] = {0.f, 0.f, 0.f, 1.f};

      mhd = (_MapHandle*)lst->data;
      if (!mhd->isBuilt)
        {
          visu_map_compute_sync(_getMap(lst->data));
          mhd->isBuilt = TRUE;
        }
      visu_map_addVertices(_getMap(lst->data), bufMap, mhd->prec, mhd->shade, mhd->alpha);
      lines = visu_map_getLines(_getMap(lst->data));
      for (i = 0; i < (lines ? lines->len : 0); i++)
        {
          float rgba[4];
          if (!mhd->color)
            {
              tool_shade_valueToRGB(mhd->shade, rgba, visu_line_getValue(g_array_index(lines, VisuLine*, i)));
              rgba[0] = 1.f - rgba[0];
              rgba[1] = 1.f - rgba[1];
              rgba[2] = 1.f - rgba[2];
              rgba[3] = 1.f;
            }
          visu_line_addVertices(g_array_index(lines, VisuLine*, i), bufLines, mhd->color ? mhd->color->rgba : rgba);
        }
      plane = visu_map_getPlane(_getMap(lst->data));
      if (!mhd->alpha && plane)
        for (GList * inter = visu_plane_getIntersection(plane); inter; inter = g_list_next(inter))
          {
            g_array_append_vals(bufFrame, black, 4);
            g_array_append_vals(bufFrame, inter->data, 3);
          }
    }

  visu_gl_ext_takeBuffer(maps, BUF_LINES, bufLines);
  visu_gl_ext_takeBuffer(maps, BUF_FRAME, bufFrame);
  visu_gl_ext_takeBuffer(maps, BUF_MAP, bufMap);

#if DEBUG == 1
  g_timer_stop(timer);
  g_debug("Extension Maps: draw map(s) in %g micro-s.",
	  g_timer_elapsed(timer, &fractionTimer)/1e-6);
  g_timer_destroy(timer);
#endif
}

static void visu_gl_ext_maps_render(const VisuGlExt *maps)
{
  if (!VISU_GL_EXT_MAPS(maps)->priv->view || !VISU_GL_EXT_MAPS(maps)->priv->maps)
    return;

  glDisable(GL_CULL_FACE);

  glEnable(GL_LINE_SMOOTH);
  glLineWidth(1.5f);
  glDepthMask(0);
  visu_gl_ext_renderBuffer(maps, BUF_FRAME);
  glDepthMask(1);
  visu_gl_ext_renderBuffer(maps, BUF_MAP);
  glLineWidth(2.f);
  glDepthMask(0);
  visu_gl_ext_renderBuffer(maps, BUF_LINES);
  glDepthMask(1);
}

/* Callbacks. */
static void visu_gl_ext_maps_rebuild(VisuGlExt *ext)
{
  VisuGlExtMaps *maps = VISU_GL_EXT_MAPS(ext);
  GList *lst;
  GError *error = (GError*)0;

  if (!visu_gl_ext_getActive(ext))
    return;
  for (lst = maps->priv->maps; lst; lst = g_list_next(lst))
    ((_MapHandle*)lst->data)->isBuilt = FALSE;

  if (!visu_gl_ext_setShaderById(ext, 0, VISU_GL_SHADER_SIMPLE, &error))
    {
      g_warning("Cannot create maps shader: %s", error->message);
      g_clear_error(&error);
    }
}
static void onViewChange(VisuGlView *view _U_, gpointer data)
{
  _setZoomLevel(VISU_GL_EXT_MAPS(data));
}
static void onMapChange(VisuMap *map, gpointer data)
{
  GList *lst;

  for (lst = VISU_GL_EXT_MAPS(data)->priv->maps; lst; lst = g_list_next(lst))
    if (((_MapHandle*)lst->data)->map == map)
      {
        g_debug("Extension Maps: map %p is dirty.", (gpointer)map);
        ((_MapHandle*)lst->data)->isBuilt = FALSE;
        break;
      }
  g_debug("Extension Maps: map changed, becoming dirty.");
  visu_gl_ext_setDirty(VISU_GL_EXT(data), VISU_GL_DRAW_REQUIRED);
}

/**
 * visu_gl_ext_maps_iter_new:
 * @maps: a #VisuGlExtMaps object.
 * @iter: (out caller-allocates): a location to #VisuGlExtMapsIter.
 *
 * Generate a new iterator to run over #VisuMap objects stored in @maps.
 *
 * Since: 3.8
 **/
void visu_gl_ext_maps_iter_new(VisuGlExtMaps *maps, VisuGlExtMapsIter *iter)
{
  g_return_if_fail(VISU_IS_GL_EXT_MAPS(maps) && iter);

  iter->maps = maps;
  iter->next = maps->priv->maps;
  visu_gl_ext_maps_iter_next(iter);
}

/**
 * visu_gl_ext_maps_iter_next:
 * @iter: a #VisuGlExtMapsIter iterator.
 *
 * Go to the next #VisuMap in @iter.
 *
 * Since: 3.8
 **/
void visu_gl_ext_maps_iter_next(VisuGlExtMapsIter *iter)
{
  g_return_if_fail(iter);

  iter->valid = (iter->next != (GList*)0);
  iter->map = (iter->valid) ? _getMap(iter->next->data) : (VisuMap*)0;
  iter->next = g_list_next(iter->next);
}
