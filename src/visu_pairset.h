/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_PAIRSET_H
#define VISU_PAIRSET_H

#include <glib-object.h>

#include <visu_data.h>
#include <visu_pairs.h>

G_BEGIN_DECLS

/**
 * VISU_TYPE_PAIR_SET:
 *
 * return the type of #VisuPairSet.
 */
#define VISU_TYPE_PAIR_SET	     (visu_pair_set_get_type ())
/**
 * VISU_PAIR_SET:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuPairSet type.
 */
#define VISU_PAIR_SET(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_PAIR_SET, VisuPairSet))
/**
 * VISU_PAIR_SET_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuPairSetClass.
 */
#define VISU_PAIR_SET_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_PAIR_SET, VisuPairSetClass))
/**
 * VISU_IS_PAIR_SET:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuPairSet object.
 */
#define VISU_IS_PAIR_SET(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_PAIR_SET))
/**
 * VISU_IS_PAIR_SET_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuPairSetClass class.
 */
#define VISU_IS_PAIR_SET_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_PAIR_SET))
/**
 * VISU_PAIR_SET_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_PAIR_SET_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_PAIR_SET, VisuPairSetClass))

typedef struct _VisuPairSetClass VisuPairSetClass;
typedef struct _VisuPairSet VisuPairSet;
typedef struct _VisuPairSetPrivate VisuPairSetPrivate;

/**
 * visu_pair_set_get_type:
 *
 * This method returns the type of #VisuPairSet, use VISU_TYPE_PAIR_SET instead.
 *
 * Returns: the type of #VisuPairSet.
 */
GType visu_pair_set_get_type(void);

/**
 * _VisuPairSet:
 * @parent: parent.
 * @priv: private data.
 *
 * This structure describes a set of pairs.
 */

struct _VisuPairSet
{
  VisuObject parent;

  VisuPairSetPrivate *priv;
};

struct _VisuPairSetClass
{
  VisuObjectClass parent;
};

VisuPairSet* visu_pair_set_new();
gboolean visu_pair_set_setModel(VisuPairSet *set, VisuData *nodes);

VisuPair* visu_pair_set_getNthPair(VisuPairSet *set, guint pos);
VisuPair* visu_pair_set_get(VisuPairSet *set,
                            const VisuElement *ele1, const VisuElement *ele2);
VisuPair* visu_pair_set_getFromLink(VisuPairSet *set, const VisuPairLink *link);

typedef struct _VisuPairSetIter VisuPairSetIter;
struct _VisuPairSetIter
{
  VisuPairSet *set;
  VisuPair *pair;
  VisuPairLink *link;

  /*< private >*/
  guint i, j;
  gboolean all;
};
void visu_pair_set_iter_new(VisuPairSet *set, VisuPairSetIter *iter, gboolean all);
void visu_pair_set_iter_next(VisuPairSetIter *iter);


/**
 * VisuPairPoolForeachFunc:
 * @pair: a #VisuPair object.
 * @user_data: some user defined data.
 *
 * Prototype of functions called with the foreach method apply to each
 * pairs.
 */
typedef void (*VisuPairPoolForeachFunc)(VisuPair *pair, gpointer user_data);
void visu_pair_pool_foreach(VisuPairPoolForeachFunc whatToDo, gpointer user_data);
void visu_pair_pool_finalize();

gboolean visu_pair_pool_readLinkFromLabel(const gchar *label, VisuPairLink **data,
                                          gchar **errorMessage);

G_END_DECLS

#endif
