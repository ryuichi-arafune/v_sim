/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include <glib.h>
#include <string.h>

#include "toolPhysic.h"

#include <visu_tools.h>

/**
 * SECTION:toolPhysic
 * @short_description: introduce physical values for the chemical species.
 *
 * This is a data base associating symbol names and atomic
 * numbers. One can also get the covalent radius of chemical
 * species. It is convenient to plot bindings.
 *
 * Since: 3.4
 */

struct periodic_table
{
  gchar *name;
  float radcov;
  float mass;
};

#define NUMBER_OF_ELEMENTS 103
static struct periodic_table eles[NUMBER_OF_ELEMENTS] = {
  {"H", 0.32, 1.008},
  {"He", 0.93, 4.0026},
  {"Li", 1.23, 6.94},
  {"Be", 0.90, 9.0122},
  {"B", 0.80, 10.81},
  {"C", 0.77, 12.011},
  {"N", 0.74, 14.007},
  {"O", 0.73, 15.999},
  {"F", 0.72, 18.998},
  {"Ne", 0.71, 20.180},
  {"Na", 1.54, 22.990},
  {"Mg", 1.36, 24.305},
  {"Al", 1.18, 26.982},
  {"Si", 1.11, 28.085},
  {"P", 1.06, 30.974},
  {"S", 1.02, 32.06},
  {"Cl", 0.99, 35.45},
  {"Ar", 0.98, 39.948},
  {"K", 2.03, 39.098},
  {"Ca", 1.74, 40.078},
  {"Sc", 1.44, 44.956},
  {"Ti", 1.32, 47.867},
  {"V", 1.22, 50.942},
  {"Cr", 1.18, 51.996},
  {"Mn", 1.17, 54.938},
  {"Fe", 1.17, 55.845},
  {"Co", 1.16, 58.933},
  {"Ni", 1.15, 58.693},
  {"Cu", 1.17, 63.546},
  {"Zn", 1.25, 65.38},
  {"Ga", 1.26, 69.723},
  {"Ge", 1.22, 72.630},
  {"As", 1.20, 74.922},
  {"Se", 1.16, 78.971},
  {"Br", 1.14, 79.904},
  {"Kr", 1.12, 83.798},
  {"Rb", 2.16, 85.468},
  {"Sr", 1.91, 87.62},
  {"Y", 1.62, 88.906},
  {"Zr", 1.45, 91.224},
  {"Nb", 1.34, 92.906},
  {"Mo", 1.30, 95.95},
  {"Tc", 1.27, 98.},
  {"Ru", 1.25, 101.07},
  {"Rh", 1.25, 102.91},
  {"Pd", 1.28, 106.42},
  {"Ag", 1.34, 107.87},
  {"Cd", 1.48, 112.41},
  {"In", 1.44, 114.82},
  {"Sn", 1.41, 118.71},
  {"Sb", 1.40, 121.76},
  {"Te", 1.36, 127.60},
  {"I", 1.33, 126.90},
  {"Xe", 1.31, 131.29},
  {"Cs", 2.35, 132.91},
  {"Ba", 1.98, 137.33},
  {"La", 1.69, 138.91},
  {"Ce", 1.65, 140.12},
  {"Pr", 1.65, 140.91},
  {"Nd", 1.64, 144.24},
  {"Pm", 1.64, 145.},
  {"Sm", 1.62, 150.36},
  {"Eu", 1.85, 151.96},
  {"Gd", 1.61, 157.25},
  {"Tb", 1.59, 158.93},
  {"Dy", 1.59, 162.50},
  {"Ho", 1.57, 164.93},
  {"Er", 1.57, 167.26},
  {"Tm", 1.56, 168.93},
  {"Yb", 1.70, 173.05},
  {"Lu", 1.56, 174.97},
  {"Hf", 1.44, 178.49},
  {"Ta", 1.34, 180.95},
  {"W", 1.30, 183.84},
  {"Re", 1.28, 186.21},
  {"Os", 1.26, 190.23},
  {"Ir", 1.27, 192.22},
  {"Pt", 1.30, 195.08},
  {"Au", 1.34, 196.97},
  {"Hg", 1.49, 200.59},
  {"Tl", 1.48, 204.38},
  {"Pb", 1.47, 207.2},
  {"Bi", 1.46, 208.98},
  {"Po", 1.46, 209.},
  {"At", 1.45, 210.},
  {"Rn", 1.45, 222.},
  {"Fr", 2.50, 223.},
  {"Ra", 2.10, 226.},
  {"Ac", 1.85, 227.},
  {"Th", 1.65, 232.04},
  {"Pa", 1.50, 231.04},
  {"U", 1.42, 238.03},
  {"Np", 1.42, 237.},
  {"Pu", 1.42, 244.},
  {"Am", 1.42, 243.},
  {"Cm", 1.42, 247.},
  {"Bk", 1.42, 247.},
  {"Cf", 1.42, 251.},
  {"Es", 1.42, 252.},
  {"Fm", 1.42, 257.},
  {"Md", 1.42, 258.},
  {"No", 1.42, 259.},
  {"Lr", 1.42, 266.}
};


/**
 * tool_physic_getSymbolFromZ:
 * @name: (out) (allow-none): a pointer on an unallocated string (can be NULL) ;
 * @radcov: (out) (allow-none): a pointer on a float (can be NULL) ;
 * @mass: (out) (allow-none): a pointer on a float (can be NULL) ;
 * @zele: the atomic number.
 *
 * Get the symbol or the covalence radius of the argument @zele.
 *
 * Returns: TRUE if zele is known in the atomic built-in list.
 */
gboolean tool_physic_getSymbolFromZ(gchar **name, float *radcov, float *mass, int zele)
{
  g_return_val_if_fail(zele > 0 && zele < NUMBER_OF_ELEMENTS + 1, FALSE);

  if (name)
    *name = eles[zele - 1].name;
  if (radcov)
    *radcov = eles[zele - 1].radcov;
  if (mass)
    *mass = eles[zele - 1].mass;

  return TRUE;
}
/**
 * tool_physic_getZFromSymbol:
 * @zele: (out) (allow-none): a pointer on an integer (can be NULL) ;
 * @radcov: (out) (allow-none): a pointer on a float (can be NULL) ;
 * @mass: (out) (allow-none): a pointer on a float (can be NULL) ;
 * @symbol: the symbol of an atom.
 *
 * Get the the covalence radius or the atomic number of a given atomic
 * @symbol.
 *
 * Returns: TRUE if @symbol is known in the atomic built-in list.
 */
gboolean tool_physic_getZFromSymbol(int *zele, float *radcov, float *mass, const gchar *symbol)
{
  int i;

  for (i = 0; i < NUMBER_OF_ELEMENTS; i++)
    {
      if (!g_strcmp0(symbol, eles[i].name))
	{
	  if (radcov)
	    *radcov = eles[i].radcov;
	  if (mass)
	    *mass = eles[i].mass;
	  if (zele)
	    *zele = i + 1;
	  return TRUE;
	}
    }
  return FALSE;
}

static const gchar* unitNames[TOOL_UNITS_N_VALUES + 1] = {"undefined", "bohr", "angstroem", "nanometer", NULL};
static const gchar* unitLabels[TOOL_UNITS_N_VALUES + 1] = {"", "bohr", "\303\205", "nm", NULL};
static const gchar* unitNamesAll[TOOL_UNITS_N_VALUES + 1][8] =
  {{"undefined", NULL, NULL, NULL, NULL, NULL, NULL, NULL},
   {"bohr", "Bohr", "bohrd0", "Bohrd0", "atomic", "Atomic", "atomicd0", "Atomicd0"},
   {"angstroem", "Angstroem", "angstroemd0", "Angstroemd0", "angstrom", "Angstrom", NULL, NULL},
   {"nanometer", "Nanometer", "nanometerd0", "Nanometerd0", NULL, NULL, NULL, NULL},
   {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}};
/**
 * tool_physic_getUnitNames:
 *
 * It provides the names corresponding to each units.
 *
 * Since: 3.5
 *
 * Returns: (transfer none) (array zero-terminated=1): an array, null
 * terminated of strings. It is owned by V_Sim.
 */
const gchar** tool_physic_getUnitNames(void)
{
  return unitNames;
}
/**
 * tool_physic_getUnitLabel:
 * @unit: a given unit.
 *
 * This routine provides the label representing the unit, not its
 * name, see tool_physic_getUnitNames().
 *
 * Since: 3.8
 *
 * Returns: a UTF8 string, not translated.
 **/
const gchar* tool_physic_getUnitLabel(ToolUnits unit)
{
  g_return_val_if_fail(unit < TOOL_UNITS_N_VALUES, NULL);

  return unitLabels[unit];
}
static float unitValues[TOOL_UNITS_N_VALUES] = {-1.f, 5.291772108e-11f, 1e-10f, 1e-9f};
/**
 * tool_physic_getUnitValueInMeter:
 * @unit: a #ToolUnits.
 *
 * It provides the factor used to transform @unit into meters.
 *
 * Since: 3.5
 *
 * Returns: a factor.
 */
float tool_physic_getUnitValueInMeter(ToolUnits unit)
{
  g_return_val_if_fail(unit != TOOL_UNITS_UNDEFINED && unit != TOOL_UNITS_N_VALUES, -1.f);

  g_debug("Visu Tools: get unit (%d) length.", unit);
  return unitValues[unit];
}
/**
 * tool_physic_getUnitConversionFactor:
 * @from: a #ToolUnits.
 * @to: a #ToolUnits.
 *
 * Retrieve the factor used to convert from @from to @to.
 *
 * Since: 3.8
 *
 * Returns: a factor.
 **/
float tool_physic_getUnitConversionFactor(ToolUnits from, ToolUnits to)
{
  g_return_val_if_fail(from < TOOL_UNITS_N_VALUES && to < TOOL_UNITS_N_VALUES, 1.f);

  if (from == TOOL_UNITS_UNDEFINED || to == TOOL_UNITS_UNDEFINED)
    return 1.f;

  return unitValues[from] / unitValues[to];
}
/**
 * tool_physic_getUnitFromName:
 * @name: a unit name.
 *
 * Find the unit corresponding to the @name. If none is found,
 * #TOOL_UNITS_UNDEFINED is returned.
 *
 * Since: 3.5
 *
 * Returns: a #ToolUnits.
 */
ToolUnits tool_physic_getUnitFromName(const gchar *name)
{
  int i, j;

  g_debug("Tool Physic: testing '%s' as a unit name.", name);
  for (i = 0; i < TOOL_UNITS_N_VALUES; i++)
    for (j = 0; j < 8 && unitNamesAll[i][j]; j++)
      if (!g_strcmp0(name, unitNamesAll[i][j]))
	return i;
  
  return TOOL_UNITS_UNDEFINED;
}
