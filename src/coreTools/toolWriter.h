/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2020)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2020)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef TOOL_WRITER_H
#define TOOL_WRITER_H

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define TOOL_TYPE_WRITER	    (tool_writer_get_type ())
#define TOOL_WRITER(obj)	    (G_TYPE_CHECK_INSTANCE_CAST(obj, TOOL_TYPE_WRITER, ToolWriter))
#define TOOL_WRITER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, TOOL_TYPE_WRITER, ToolWriterClass))
#define TOOL_IS_WRITER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE(obj, TOOL_TYPE_WRITER))
#define TOOL_IS_WRITER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, TOOL_TYPE_WRITER))
#define TOOL_WRITER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, TOOL_TYPE_WRITER, ToolWriterClass))

/**
 * ToolWriterFontSize:
 * @TOOL_WRITER_FONT_NORMAL: normal size font,
 * @TOOL_WRITER_FONT_SMALL: smaller font size.
 *
 * Flags to govern the rendering font size of a #ToolWriter object.
 */
typedef enum
{
  TOOL_WRITER_FONT_NORMAL,
  TOOL_WRITER_FONT_SMALL
} ToolWriterFontSize;

typedef struct _ToolWriterPrivate ToolWriterPrivate;
typedef struct _ToolWriter ToolWriter;
struct _ToolWriter
{
  GObject parent;

  ToolWriterPrivate *priv;
};

/**
 * ToolWriterClass:
 * @parent: the parent class.
 *
 * A short way to identify #_ToolWriterClass structure.
 */
typedef struct _ToolWriterClass ToolWriterClass;
struct _ToolWriterClass
{
  GObjectClass parent;
};

/**
 * tool_writer_get_type:
 *
 * This method returns the type of #ToolWriter, use TOOL_TYPE_WRITER instead.
 *
 * Returns: the type of #ToolWriter.
 */
GType tool_writer_get_type(void);

ToolWriter* tool_writer_getStatic(void);
void tool_writer_setSize(ToolWriter *writer, ToolWriterFontSize size);
void tool_writer_setStroke(ToolWriter *writer, gboolean stroke);
GArray* tool_writer_layout(const ToolWriter *writer, const gchar *text,
                           guint *width, guint *height);

G_END_DECLS

#endif
