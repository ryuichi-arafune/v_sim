/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "toolMatrix.h"

#include <math.h>
#include <visu_configFile.h>
#include "toolConfigFile.h"

/**
 * SECTION:toolMatrix
 * @short_description: Defines basic handlings on matrix.
 *
 * <para>Some very basic linear algebra are redefined here. It also
 * gives access to coordinates conversion, essentially between
 * cartesian and spherical.</para>
 */

/**
 * VisuBoxVertices:
 * @vertices: (array fixed-size=8) (element-type ToolVector):
 *
 * Structure used for bindings.
 *
 * Since: 3.7
 */

/**
 * VisuBoxCell:
 * @box: (array fixed-size=6) (element-type gdouble):
 *
 * Structure used for bindings.
 *
 * Since: 3.7
 */

/**
 * ToolVector:
 *
 * An opaque object, to exchange three floats as #GObject properties.
 *
 * Since: 3.8
 */

/**
 * ToolGridSize:
 * @grid: (array fixed-size=3) (element-type guint):
 *
 * Structure used for bindings.
 *
 * Since: 3.7
 */

/**
 * tool_matrix_reducePrimitiveVectors:
 * @reduced: (out caller-allocates) (array fixed-size=6): a storage for 6 floating point values ;
 * @full: (in) (array fixed-size=9): a full 3x3 matrix to be transformed.
 *
 * This routine transforms the given matrix @full into a reduced array
 * used by V_Sim to store box definition.
 *
 * Returns: FALSE if the given matrix is planar.
 */
gboolean tool_matrix_reducePrimitiveVectors(double reduced[6], double full[3][3])
{
  double X[3];
  double Y[3];
  double Z[3];
  double u[3], x[3];
  int i, j, k;
  double deltaIJ;
  double norm;

  g_return_val_if_fail(reduced && full, FALSE);

  g_debug("Matrix: transform full to reduced matrix.");
  g_debug("Matrix: full is  %8g %8g %8g\n"
              "                 %8g %8g %8g\n"
	      "                 %8g %8g %8g",
	      full[0][0], full[0][1], full[0][2],
	      full[1][0], full[1][1], full[1][2],
	      full[2][0], full[2][1], full[2][2]);
  /* Compute the X vector of the new basis, colinear with old x. */
  for (i = 0; i < 3; i++)
    {
      X[i] = full[0][i];
      x[i] = full[0][i];
    }

  /* Compute the Y vector of the new basis, orthogonal to X and
     coplanar with X and old y vector. */
  u[0] = full[0][1] * full[1][2] - full[0][2] * full[1][1];
  u[1] = full[0][2] * full[1][0] - full[0][0] * full[1][2];
  u[2] = full[0][0] * full[1][1] - full[0][1] * full[1][0];
/*   g_debug("x        : %f %f %f", x[0], x[1], x[2]); */
/*   g_debug("x vect y : %f %f %f", u[0], u[1], u[2]); */
  deltaIJ = x[0] * u[1] - x[1] * u[0];
  if (deltaIJ != 0.)
    {
      i = 0;
      j = 1;
      k = 2;
      g_debug(" Using deltaIJ scheme with (i, j, k)"
		  " = (%d, %d, %d)", i, j, k);
    }
  else
    {
      deltaIJ = x[0] * u[2] - x[2] * u[0];
      if (deltaIJ != 0.)
	{
	  i = 0;
	  j = 2;
	  k = 1;
	  g_debug(" Using deltaIJ scheme with (i, j, k)"
		      " = (%d, %d, %d)", i, j, k);
	}
      else
	{
	  deltaIJ = x[1] * u[2] - x[2] * u[1];
	  if (deltaIJ != 0.)
	    {
	      i = 1;
	      j = 2;
	      k = 0;
	      g_debug(" Using deltaIJ scheme with (i, j, k)"
			  " = (%d, %d, %d)", i, j, k);
	    }
	  else
            return FALSE;
	}
    }
  Y[k] = -1.;
  Y[i] = (x[k] * u[j] - x[j] * u[k]) / deltaIJ;
  Y[j] = (x[i] * u[k] - x[k] * u[i]) / deltaIJ;
  /* We need to turn Y if y.Y is negative. */
  norm = 0.;
  for (i = 0; i < 3; i++)
    norm += full[1][i] * Y[i];
  if (norm < 0.)
  for (i = 0; i < 3; i++)
    Y[i] *= -1.;
    
  /* Compute the new Z vector in order to form a direct orthogonal
     basis with X and Y. */
  Z[0] = X[1] * Y[2] - X[2] * Y[1];
  Z[1] = X[2] * Y[0] - X[0] * Y[2];
  Z[2] = X[0] * Y[1] - X[1] * Y[0];

  /* Normalise the new basis (X, Y, Z). */
  norm = 0.;
  for (i = 0; i < 3; i++)
    norm += X[i] * X[i];
  norm = sqrt(norm);
  for (i = 0; i < 3; i++)
    X[i] /= norm;
  norm = 0.;
  for (i = 0; i < 3; i++)
    norm += Y[i] * Y[i];
  norm = sqrt(norm);
  for (i = 0; i < 3; i++)
    Y[i] /= norm;
  norm = 0.;
  for (i = 0; i < 3; i++)
    norm += Z[i] * Z[i];
  norm = sqrt(norm);
  for (i = 0; i < 3; i++)
    Z[i] /= norm;

/*   g_debug("X : %f %f %f", X[0], X[1], X[2]); */
/*   g_debug("Y : %f %f %f", Y[0], Y[1], Y[2]); */
/*   g_debug("Z : %f %f %f", Z[0], Z[1], Z[2]); */

  /* Compute the reduce value for the basis. */
  g_debug(" Write to reduced (%p).", (gpointer)reduced);
  reduced[0] = 0.;
  for (i = 0; i < 3; i++)
    reduced[0] += X[i] * full[0][i];

  reduced[1] = 0.;
  for (i = 0; i < 3; i++)
    reduced[1] += X[i] * full[1][i];

  reduced[2] = 0.;
  for (i = 0; i < 3; i++)
    reduced[2] += Y[i] * full[1][i];

  reduced[3] = 0.;
  for (i = 0; i < 3; i++)
    reduced[3] += X[i] * full[2][i];

  reduced[4] = 0.;
  for (i = 0; i < 3; i++)
    reduced[4] += Y[i] * full[2][i];

  reduced[5] = 0.;
  for (i = 0; i < 3; i++)
    reduced[5] += Z[i] * full[2][i];
  g_debug(" Write OK.");

  return TRUE;
}

/**
 * tool_matrix_dtof:
 * @mf: a matrix in single precision.
 * @md: a matrix in double precision.
 *
 * Cast @md into @mf.
 *
 * Since: 3.7
 **/
void tool_matrix_dtof(float mf[3][3], double md[3][3])
{
  int i, j;

  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      mf[i][j] = md[i][j];
}

/**
 * tool_matrix_setIdentity:
 * @mat: (array fixed-size=9): a matrix location.
 *
 * Initialise @mat with the identity.
 *
 * Since: 3.7
 **/
void tool_matrix_setIdentity(float mat[3][3])
{
  mat[0][0] = 1.f;
  mat[0][1] = 0.f;
  mat[0][2] = 0.f;
  mat[1][0] = 0.f;
  mat[1][1] = 1.f;
  mat[1][2] = 0.f;
  mat[2][0] = 0.f;
  mat[2][1] = 0.f;
  mat[2][2] = 1.f;
}
/**
 * tool_matrix_set:
 * @mat: a matrix.
 * @orig: a matrix.
 *
 * Copy @orig into @mat.
 *
 * Since: 3.8
 **/
void tool_matrix_set(float mat[3][3], float orig[3][3])
{
  mat[0][0] = orig[0][0];
  mat[0][1] = orig[0][1];
  mat[0][2] = orig[0][2];
  mat[1][0] = orig[1][0];
  mat[1][1] = orig[1][1];
  mat[1][2] = orig[1][2];
  mat[2][0] = orig[2][0];
  mat[2][1] = orig[2][1];
  mat[2][2] = orig[2][2];
}
/**
 * tool_matrix_rotate:
 * @mat: a matrix.
 * @angle: an angle in degrees.
 * @dir: a direction.
 *
 * Create a rotation matrix along axis @dir of the given @angle.
 *
 * Since: 3.8
 **/
void tool_matrix_rotate(float mat[3][3], float angle, ToolXyzDir dir)
{
  float rot[3][3], work[3][3];
  int perm[3][3] = {{1, 2, 0}, {2, 0, 1}, {0, 1, 2}};

  rot[perm[dir][0]][perm[dir][0]] = cos(angle * TOOL_PI180);
  rot[perm[dir][0]][perm[dir][1]] = -sin(angle * TOOL_PI180);
  rot[perm[dir][0]][perm[dir][2]] = 0.f;
  rot[perm[dir][1]][perm[dir][0]] = sin(angle * TOOL_PI180);
  rot[perm[dir][1]][perm[dir][1]] = cos(angle * TOOL_PI180);
  rot[perm[dir][1]][perm[dir][2]] = 0.f;
  rot[perm[dir][2]][perm[dir][0]] = 0.f;
  rot[perm[dir][2]][perm[dir][1]] = 0.f;
  rot[perm[dir][2]][perm[dir][2]] = 1.f;

  tool_matrix_set(work, mat);
  tool_matrix_productMatrix(mat, work, rot);
}

/**
 * tool_matrix_productMatrix:
 * @matRes: an array of floating point values of size 3x3 ;
 * @matA: an array of floating point values of size 3x3 ;
 * @matB: an array of floating point values of size 3x3.
 *
 * Compute the mathematical product between @matA and @matB and
 * put the result matrix in @matRes.
 *
 * Since: 3.2
 */
void tool_matrix_productMatrix(float matRes[3][3], float matA[3][3], float matB[3][3])
{
  int i, j, k;

  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      {
	matRes[i][j] = 0.;
	for (k = 0; k < 3; k++)
	  matRes[i][j] += matA[i][k] * matB[k][j];
      }
}
/**
 * tool_matrix_productVector:
 * @vectRes: an array of floating point values of size 3 ;
 * @mat: an array of floating point values of size 3x3 ;
 * @vect: an array of floating point values of size 3.
 *
 * Compute the mathematical product between @matA and @vect and
 * put the result vector in @vectRes.
 *
 * Since: 3.2
 */
void tool_matrix_productVector(float vectRes[3], float mat[3][3], float vect[3])
{
  int i, j;

  for (i = 0; i < 3; i++)
    {
      vectRes[i] = 0.;
      for (j = 0; j < 3; j++)
	vectRes[i] += mat[i][j] * vect[j];
    }
}
/**
 * tool_matrix_determinant:
 * @mat: a matrix.
 *
 * Calculate the determinant of matrix @mat.
 *
 * Since: 3.6
 *
 * Returns: the determinant value.
 */
float tool_matrix_determinant(float mat[3][3])
{
  g_debug("Tool Matrix: %g is det( %8g %8g %8g\n"
	              "                        %8g %8g %8g\n"
	              "                        %8g %8g %8g )",
	      mat[0][0] * (mat[1][1] * mat[2][2] - mat[1][2] * mat[2][1]) -
              mat[0][1] * (mat[1][0] * mat[2][2] - mat[1][2] * mat[2][0]) +
              mat[0][2] * (mat[1][0] * mat[2][1] - mat[1][1] * mat[2][0]),
              mat[0][0], mat[0][1], mat[0][2],
	      mat[1][0], mat[1][1], mat[1][2],
	      mat[2][0], mat[2][1], mat[2][2]);

  return mat[0][0] * (mat[1][1] * mat[2][2] - mat[1][2] * mat[2][1]) -
    mat[0][1] * (mat[1][0] * mat[2][2] - mat[1][2] * mat[2][0]) +
    mat[0][2] * (mat[1][0] * mat[2][1] - mat[1][1] * mat[2][0]);
}
/**
 * tool_matrix_invert:
 * @inv: a matrix (out values).
 * @mat: a matrix.
 *
 * Calculate the inverse matrix of matrix @mat and store it in @inv.
 *
 * Since: 3.6
 *
 * Returns: FALSE if @mat is singular.
 */
gboolean tool_matrix_invert(float inv[3][3], float mat[3][3])
{
  float det;

  det = tool_matrix_determinant(mat);
  if (det == 0.f)
    return FALSE;

  det = 1.f / det;

  inv[0][0] = det * (mat[1][1] * mat[2][2] - mat[1][2] * mat[2][1]);
  inv[0][1] = det * (mat[0][2] * mat[2][1] - mat[0][1] * mat[2][2]);
  inv[0][2] = det * (mat[0][1] * mat[1][2] - mat[0][2] * mat[1][1]);

  inv[1][0] = det * (mat[1][2] * mat[2][0] - mat[1][0] * mat[2][2]);
  inv[1][1] = det * (mat[0][0] * mat[2][2] - mat[0][2] * mat[2][0]);
  inv[1][2] = det * (mat[0][2] * mat[1][0] - mat[0][0] * mat[1][2]);

  inv[2][0] = det * (mat[1][0] * mat[2][1] - mat[1][1] * mat[2][0]);
  inv[2][1] = det * (mat[0][1] * mat[2][0] - mat[0][0] * mat[2][1]);
  inv[2][2] = det * (mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0]);

  return TRUE;
}
/**
 * tool_matrix_getRotationFromFull:
 * @rot: a rotation matrix (out values).
 * @full: the description of basis set in full development.
 * @box: the description of basis set in align X axis.
 *
 * There is a rotation matrix to transform from full cartesian
 * coordinates into reduced box cartesian coordinates.
 *
 * Since: 3.6
 *
 * Returns: TRUE if @full does not describe properly a 3D box.
 */
gboolean tool_matrix_getRotationFromFull(float rot[3][3],
				       double full[3][3], double box[6])
{
  float boxMat[3][3], fileMat[3][3], fileMatInv[3][3];

  /* We create the rotation matrix that pass from the cartesian
     coordinates of the file to the cartesian coordinates of the box. */
  boxMat[0][0] = box[0];
  boxMat[0][1] = box[1];
  boxMat[0][2] = box[3];
  boxMat[1][0] = 0.f;
  boxMat[1][1] = box[2];
  boxMat[1][2] = box[4];
  boxMat[2][0] = 0.f;
  boxMat[2][1] = 0.f;
  boxMat[2][2] = box[5];
  fileMat[0][0] = (float)full[0][0];
  fileMat[0][1] = (float)full[1][0];
  fileMat[0][2] = (float)full[2][0];
  fileMat[1][0] = (float)full[0][1];
  fileMat[1][1] = (float)full[1][1];
  fileMat[1][2] = (float)full[2][1];
  fileMat[2][0] = (float)full[0][2];
  fileMat[2][1] = (float)full[1][2];
  fileMat[2][2] = (float)full[2][2];
  if (!tool_matrix_invert(fileMatInv, fileMat))
    return FALSE;

  tool_matrix_productMatrix(rot, boxMat, fileMatInv);
  g_debug("Tool Matrix: rotation matrix %8g %8g %8g\n"
	              "                             %8g %8g %8g\n"
	              "                             %8g %8g %8g",
	      rot[0][0], rot[0][1], rot[0][2],
	      rot[1][0], rot[1][1], rot[1][2],
	      rot[2][0], rot[2][1], rot[2][2]);
  return TRUE;
}

#ifndef RAD2DEG
#define RAD2DEG(x) (57.29577951308232311 * x)
#endif

#ifndef DEG2RAD
#define DEG2RAD(x) (0.01745329251994329509 * x)
#endif

/**
 * tool_matrix_cartesianToSpherical:
 * @spherical: an allocated array of 3 floating point values to store the result ;
 * @cartesian: an allocated array of 3 floating point values to read the input.
 *
 * A method to transform cartesian coordinates in spherical
 * coordinates (radius, phi and theta).
 *
 * Since: 3.3
 */
void tool_matrix_cartesianToSpherical(float spherical[3], const float cartesian[3])
{
/* s[0] = rho, s[1] = theta, s[2] = phi
   c[0] = x, c[1] = y, c[2] = z */
  double rho;
  double theta; 
  double phi;

  if (cartesian[0] == 0 && cartesian[1] == 0 && cartesian[2] == 0)
    {
      spherical[TOOL_MATRIX_SPHERICAL_MODULUS] = 0;
      spherical[TOOL_MATRIX_SPHERICAL_THETA] = 0;
      spherical[TOOL_MATRIX_SPHERICAL_PHI] = 0;
      return;
    }

  rho = sqrt(cartesian[0] * cartesian[0] +
             cartesian[1] * cartesian[1] +
             cartesian[2] * cartesian[2]);

  if (cartesian[0] == 0 && cartesian[1] == 0)
    theta = (cartesian[2] > 0) ? 0 : G_PI;
  else 
    theta = acos(CLAMP(cartesian[2] / rho, -1., 1.));

  if (cartesian[0] != 0)
    {
      phi = atan(cartesian[1] / cartesian[0]) + G_PI*((cartesian[0] < 0) ? 1 : 0);
    }
  else
    {
      if (cartesian[1] == 0) /* facultatif*/
	phi = 0;    /* facultatif*/
      else if(cartesian[1] > 0)
	phi = G_PI_2;
      else
	phi = -G_PI_2;
    }

  spherical[TOOL_MATRIX_SPHERICAL_MODULUS] = rho;
  spherical[TOOL_MATRIX_SPHERICAL_THETA] = RAD2DEG(theta);
  spherical[TOOL_MATRIX_SPHERICAL_PHI] = tool_modulo_float(RAD2DEG(phi), 360);
}

/**
 * tool_matrix_sphericalToCartesian:
 * @cartesian: an allocated array of 3 floating point values to store the result ;
 * @spherical: an allocated array of 3 floating point values to read the input.
 *
 * A method to transform spherical coordinates (radius, phi and theta)
 * to cartesian coordinates.
 *
 * Since: 3.3
 */
void tool_matrix_sphericalToCartesian(float cartesian[3], const float spherical[3])
{
  cartesian[0] = spherical[TOOL_MATRIX_SPHERICAL_MODULUS] *
    sin(DEG2RAD(spherical[TOOL_MATRIX_SPHERICAL_THETA])) *
    cos(DEG2RAD(spherical[TOOL_MATRIX_SPHERICAL_PHI]));
  cartesian[1] = spherical[TOOL_MATRIX_SPHERICAL_MODULUS] *
    sin(DEG2RAD(spherical[TOOL_MATRIX_SPHERICAL_THETA])) *
    sin(DEG2RAD(spherical[TOOL_MATRIX_SPHERICAL_PHI]));
  cartesian[2] = spherical[TOOL_MATRIX_SPHERICAL_MODULUS] *
    cos(DEG2RAD(spherical[TOOL_MATRIX_SPHERICAL_THETA]));
}

/**
 * tool_matrix_getInter2D:
 * @lambda: a location to store a float.
 * @a: a point.
 * @b: another point.
 * @A: a point.
 * @B: another point.
 *
 * Get the intersection coeeficient of lines [ab] and [AB].
 *
 * Returns: TRUE if [ab] and [AB] have an intersection.
 */
gboolean tool_matrix_getInter2D(float *lambda,
			   float a[2], float b[2], float A[2], float B[2])
{
  float denom;

  denom = (b[0] - a[0]) * (B[1] - A[1]) - (b[1] - a[1]) * (B[0] - A[0]);
  if (denom == 0.f)
    return FALSE;
  *lambda = (A[0] - a[0]) * (B[1] - A[1]) - (A[1] - a[1]) * (B[0] - A[0]);
  *lambda /= denom;
/*   g_debug("%g", *lambda); */
  return TRUE;
}
/**
 * tool_matrix_getInter2DFromList: (skip)
 * @i: a location to store a point.
 * @lambda: a location to store a float.
 * @a: a point.
 * @b: another point.
 * @set: a list of points.
 *
 * Same as tool_matrix_getInter2D(), but from a list of points.
 *
 * Returns: TRUE if an intersection exists.
 */
gboolean tool_matrix_getInter2DFromList(float i[2], float *lambda,
                                        float a[2], float b[2], GList *set)
{
  float *pt1, *pt2;
  float l, min;
  
  i[0] = a[0];
  i[1] = a[1];

  min = 1.2f;
  for (pt1 = (float*)(g_list_last(set)->data); set; set = g_list_next(set))
    {
      pt2 = (float*)set->data;
      if (tool_matrix_getInter2D(&l, a, b, pt1, pt2))
	min = (l >= 0.f)?MIN(min, l):min;
      pt1 = pt2;
    }
  if (min > 1.00001f)
    return FALSE;
  if (lambda)
    *lambda = min;
  i[0] = (b[0] - a[0]) * min + a[0];
  i[1] = (b[1] - a[1]) * min + a[1];
/*   g_debug("%g -> %gx%g", min, i[0], i[1]); */
  return TRUE;
}

#define FLAG_PARAMETER_THRESHOLD "scale_log_threshold"
#define DESC_PARAMETER_THRESHOLD "Value of the threshold used in the zero centred TOOL_MATRIX_SCALING_LOG scaling function ; a positive float (1e-3)"
static float threshold = 1e-3;
static void exportParameters(GString *data, VisuData *dataObj);

/**
 * tool_matrix_getScaledLinear:
 * @x: the initial value ;
 * @minmax: the boundaries for the @x argument ;
 *
 * Transform @x into [0;1] with a linear scale.
 *
 * Returns: a value into [0;1].
 *
 * Since: 3.5
 */
float tool_matrix_getScaledLinear(float x, float minmax[2])
{
  return ((CLAMP(x, minmax[0], minmax[1]) - minmax[0]) /
	  (minmax[1] - minmax[0]));
}
/**
 * tool_matrix_getScaledLog:
 * @x: the initial value ;
 * @minmax: the boundaries for the @x argument.
 *
 * Transform @x into [0;1] with a log scale.
 *
 * Returns: a value into [0;1].
 *
 * Since: 3.5
 */
float tool_matrix_getScaledLog(float x, float minmax[2])
{
  /* float v; */
  float lMinMax[2];

  lMinMax[0] = log10(MAX(1e-12, minmax[0]));
  lMinMax[1] = log10(MAX(1e-12, minmax[1]));
  return tool_matrix_getScaledLinear(log10(MAX(1e-12, x)), lMinMax);
  /* return (v == 0.)?0.: - (log10(v) - param) / param; */
}
/**
 * tool_matrix_getScaledZeroCentredLog:
 * @x: the initial value ;
 * @minmax: the boundaries for the @x argument.
 *
 * Transform @x into [0;1] with a log scale with zero centred values.
 *
 * Returns: a value into [0;1].
 *
 * Since: 3.5
 */
float tool_matrix_getScaledZeroCentredLog(float x, float minmax[2])
{
  float v, m;

  m = MAX(minmax[1], -minmax[0]);
  v = CLAMP(x, -m, m);
  return 0.5 + (v < 0.?-1.:1.) * (log(m * threshold) -
				  log(MAX(ABS(v), m * threshold))) / 
    (2. * log(threshold));
}
/**
 * tool_matrix_getScaledLinearInv:
 * @x: the initial value ;
 * @minmax: the boundaries for the @x argument.
 *
 * Reverse function for tool_matrix_getScaledLinear().
 *
 * Returns: a value into [0;1].
 *
 * Since: 3.5
 */
float tool_matrix_getScaledLinearInv(float x, float minmax[2])
{
  return (minmax[0] + CLAMP(x, 0., 1.) * (minmax[1] - minmax[0]));
}
/**
 * tool_matrix_getScaledLogInv:
 * @x: the initial value ;
 * @minmax: the boundaries for the @x argument.
 *
 * Reverse function for tool_matrix_getScaledLog().
 *
 * Returns: a value into [0;1].
 *
 * Since: 3.5
 */
float tool_matrix_getScaledLogInv(float x, float minmax[2])
{
  return MAX(1e-12, minmax[0]) * pow(MAX(1e-12, minmax[1]) / MAX(1e-12, minmax[0]), CLAMP(x, 0., 1.));
  /* return (minmax[0] + (minmax[1] - minmax[0]) * exp((1. - CLAMP(x, 0., 1.)) * param)); */
}
/**
 * tool_matrix_getScaledZeroCentredLogInv:
 * @x: the initial value ;
 * @minmax: the boundaries for the @x argument.
 *
 * Reverse function for tool_matrix_getScaledZeroCentredLog().
 *
 * Returns: a value into [0;1].
 *
 * Since: 3.5
 */
float tool_matrix_getScaledZeroCentredLogInv(float x, float minmax[2])
{
  float s, m, out;

  g_debug("Matrix: get inv ZCL %g (%g-%g).", x, minmax[0], minmax[1]);
  s = (x < 0.5)?-1.:1.;
  m = MAX(minmax[1], -minmax[0]);
  out = s * m * threshold * exp(s * (1. - 2. * CLAMP(x, 0., 1.)) * log(threshold));
  g_debug(" | %g (%g)", out, tool_matrix_getScaledZeroCentredLog(out, minmax));
  return out;
}

static void exportParameters(GString *data, VisuData *dataObj _U_)
{
  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_THRESHOLD);
  g_string_append_printf(data, "%s: %f\n\n", FLAG_PARAMETER_THRESHOLD,
			 threshold);
}
/**
 * tool_matrix_init: (skip)
 *
 * This method is used by V_Sim internally and should not be called.
 *
 * Since: 3.5
 */
void tool_matrix_init(void)
{
  float rg[2] = {G_MINFLOAT, G_MAXFLOAT};
  VisuConfigFileEntry *entry;

  /* Set private variables. */
  entry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_PARAMETER,
                                              FLAG_PARAMETER_THRESHOLD,
                                              DESC_PARAMETER_THRESHOLD,
                                              1, &threshold, rg, FALSE);
  visu_config_file_entry_setVersion(entry, 3.5f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_PARAMETER,
                                     exportParameters);
}

static gpointer vector_copy(gconstpointer boxed)
{
  gfloat *vector, *orig = (gfloat*)boxed;

  if (!boxed)
    return (gpointer)0;

  vector = g_malloc(sizeof(gfloat) * 3);
  vector[0] = orig[0];
  vector[1] = orig[1];
  vector[2] = orig[2];
  return (gpointer)vector;
}
/**
 * tool_vector_get_type:
 *
 * Create and retrieve a #GType for a #ToolVector object.
 *
 * Since: 3.8
 *
 * Returns: a new type for #ToolVector structures.
 */
GType tool_vector_get_type(void)
{
  static GType g_define_type_id = 0;

  if (g_define_type_id == 0)
    g_define_type_id = g_boxed_type_register_static("ToolVector",
                                                    (GBoxedCopyFunc)vector_copy, g_free);
  return g_define_type_id;
}

/**
 * tool_vector_new:
 * @orig: (array fixed-size=3): a vector.
 *
 * Creates a new #ToolVector by copying @orig.
 *
 * Since: 3.8
 *
 * Returns: (transfer full) (type ToolVector):
 **/
float* tool_vector_new(const float orig[3])
{
  return vector_copy(orig);
}

/**
 * tool_vector_set:
 * @dest: a vector.
 * @orig: a vector.
 *
 * Copy @orig into @dest and also test if @dest was already equals to @orig.
 *
 * Since: 3.8
 *
 * Returns: TRUE if @dest is different from @orig.
 **/
gboolean tool_vector_set(float dest[3], const float orig[3])
{
  if (dest[0] == orig[0] && dest[1] == orig[1] && dest[2] == orig[2])
    return FALSE;

  dest[0] = orig[0];
  dest[1] = orig[1];
  dest[2] = orig[2];
  return TRUE;
}

/**
 * tool_vector_spherical:
 * @cart: (array fixed-size=3): a vector in cartesian coordinates.
 * @at: a spherical direction.
 *
 * Convert @cart in spherical coordinates. It is equivalent to
 * tool_matrix_cartesianToSpherical() but returns the value in a given
 * direction only. This is intended for bindings.
 *
 * Since: 3.8
 *
 * Returns: the coodinate in spherical.
 **/
float tool_vector_spherical(const float cart[3], ToolMatrixSphericalCoord at)
{
  float sph[3];

  tool_matrix_cartesianToSpherical(sph, cart);
  return sph[at];
}

static gpointer minmax_copy(gpointer boxed)
{
  gfloat *mm, *orig = (gfloat*)boxed;

  if (!boxed)
    return (gpointer)0;

  mm = g_malloc(sizeof(gfloat) * 2);
  mm[0] = orig[0];
  mm[1] = orig[1];
  return (gpointer)mm;
}
/**
 * tool_minmax_get_type:
 *
 * Create and retrieve a #GType for a #ToolMinmax object.
 *
 * Since: 3.8
 *
 * Returns: a new type for #ToolMinmax structures.
 */
GType tool_minmax_get_type(void)
{
  static GType g_define_type_id = 0;

  if (g_define_type_id == 0)
    g_define_type_id = g_boxed_type_register_static("ToolMinmax", minmax_copy, g_free);
  return g_define_type_id;
}

/**
 * tool_minmax:
 * @global: (array fixed-size=2): the global min and max values.
 * @minmax: (array fixed-size=2): some min and max values.
 *
 * Take the minimum and maximum of @global and @minmax and put them
 * into @global. @global should be initialised.
 *
 * Since: 3.8
 **/
void tool_minmax(float global[2], const float minmax[2])
{
  global[0] = MIN(minmax[0], global[0]);
  global[1] = MAX(minmax[1], global[1]);
}
/**
 * tool_minmax_fromDbl:
 * @global: (array fixed-size=2): the global min and max values.
 * @minmax: (array fixed-size=2): some min and max values.
 *
 * Same as tool_minmax() for double inputs.
 *
 * Since: 3.8
 **/
void tool_minmax_fromDbl(float global[2], const double minmax[2])
{
  global[0] = MIN(minmax[0], global[0]);
  global[1] = MAX(minmax[1], global[1]);
}

/**
 * tool_vector_nrm:
 * @v: three floats.
 *
 * Normalise @v.
 *
 * Since: 3.9
 **/
float tool_vector_nrm(float v[3])
{
  float nrm = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
  float inv = 1.f / nrm;
  v[0] *= inv;
  v[1] *= inv;
  v[2] *= inv;
  return nrm;
}

/**
 * tool_vector_prod:
 * @w: three floats
 * @u: a vector
 * @v: a vector
 *
 * Compute in @w the vector product of @u and @v.
 *
 * Since: 3.9
 **/
void tool_vector_prod(float w[3], const float u[3], const float v[3])
{
  w[0] = u[1] * v[2] - u[2] * v[1];
  w[1] = u[2] * v[0] - u[0] * v[2];
  w[2] = u[0] * v[1] - u[1] * v[0];
}

/**
 * tool_gl_matrix_setIdentity:
 * @matrix: a #ToolGlMatrix location
 *
 * Set @matrix to the identity matrix.
 *
 * Since: 3.9
 **/
void tool_gl_matrix_setIdentity(ToolGlMatrix *matrix)
{
  const ToolGlMatrix id = {{{1.f, 0.f, 0.f, 0.f},
                            {0.f, 1.f, 0.f, 0.f},
                            {0.f, 0.f, 1.f, 0.f},
                            {0.f, 0.f, 0.f, 1.f}}};
  if (matrix)
    *matrix = id;
}

/**
 * tool_gl_matrix_setOrtho:
 * @matrix: a #ToolGlMatrix location
 * @width: a width
 * @height: a height
 *
 * Construct a projection matrix corresponding to the 2D rectangle of
 * @width per @height pixels.
 *
 * Since: 3.9
 **/
void tool_gl_matrix_setOrtho(ToolGlMatrix *matrix, guint width, guint height)
{
  if (matrix)
    {
      tool_gl_matrix_setIdentity(matrix);
      matrix->c[0][0] = 2.f / width;
      matrix->c[1][1] = 2.f / height;
      matrix->c[2][2] = -2.f / 100.f;
      matrix->c[3][0] = -1.f;
      matrix->c[3][1] = -1.f;
    }
}

/**
 * tool_gl_matrix_transpose:
 * @matrix: a GL matrix
 *
 * Transpose @matrix in place.
 *
 * Since: 3.9
 **/
void tool_gl_matrix_transpose(ToolGlMatrix *matrix)
{
  gfloat buf[4];

  g_return_if_fail(matrix);

  buf[1] = matrix->c[0][1];
  buf[2] = matrix->c[0][2];
  buf[3] = matrix->c[0][3];
  matrix->c[0][1] = matrix->c[1][0];
  matrix->c[0][2] = matrix->c[2][0];
  matrix->c[0][3] = matrix->c[3][0];
  matrix->c[1][0] = buf[1];
  matrix->c[2][0] = buf[2];
  matrix->c[3][0] = buf[3];

  buf[2] = matrix->c[1][2];
  buf[3] = matrix->c[1][3];
  matrix->c[1][2] = matrix->c[2][1];
  matrix->c[1][3] = matrix->c[3][1];
  matrix->c[2][1] = buf[2];
  matrix->c[3][1] = buf[3];

  buf[3] = matrix->c[2][3];
  matrix->c[2][3] = matrix->c[3][2];
  matrix->c[3][2] = buf[3];
}

/**
 * tool_gl_matrix_vectorProd:
 * @v: a GL vector location
 * @matrix: a GL matrix
 * @u: a GL vector
 *
 * 4x4 matrix product with a vector.
 *
 * Since: 3.9
 **/
void tool_gl_matrix_vectorProd(gfloat v[4], const ToolGlMatrix *matrix, const gfloat u[4])
{
  g_return_if_fail(matrix);

  v[0] = matrix->c[0][0] * u[0] + matrix->c[1][0] * u[1] + matrix->c[2][0] * u[2] + matrix->c[3][0] * u[3];
  v[1] = matrix->c[0][1] * u[0] + matrix->c[1][1] * u[1] + matrix->c[2][1] * u[2] + matrix->c[3][1] * u[3];
  v[2] = matrix->c[0][2] * u[0] + matrix->c[1][2] * u[1] + matrix->c[2][2] * u[2] + matrix->c[3][2] * u[3];
  v[3] = matrix->c[0][3] * u[0] + matrix->c[1][3] * u[1] + matrix->c[2][3] * u[2] + matrix->c[3][3] * u[3];
}

/**
 * tool_gl_matrix_matrixProd:
 * @o: location for a #ToolGlMatrix
 * @m1: a #ToolGlMatrix
 * @m2: a #ToolGlMatrix
 *
 * Compute the product of matrix @m1 with matrix @m2 and store the
 * result in @o
 *
 * Since: 3.9
 **/
void tool_gl_matrix_matrixProd(ToolGlMatrix *o, const ToolGlMatrix *m1, const ToolGlMatrix *m2)
{
  g_return_if_fail(o && m1 && m2);

  o->c[0][0] = m1->c[0][0] * m2->c[0][0] + m1->c[1][0] * m2->c[0][1] + m1->c[2][0] * m2->c[0][2] + m1->c[3][0] * m2->c[0][3];
  o->c[0][1] = m1->c[0][1] * m2->c[0][0] + m1->c[1][1] * m2->c[0][1] + m1->c[2][1] * m2->c[0][2] + m1->c[3][1] * m2->c[0][3];
  o->c[0][2] = m1->c[0][2] * m2->c[0][0] + m1->c[1][2] * m2->c[0][1] + m1->c[2][2] * m2->c[0][2] + m1->c[3][2] * m2->c[0][3];
  o->c[0][3] = m1->c[0][3] * m2->c[0][0] + m1->c[1][3] * m2->c[0][1] + m1->c[2][3] * m2->c[0][2] + m1->c[3][3] * m2->c[0][3];

  o->c[1][0] = m1->c[0][0] * m2->c[1][0] + m1->c[1][0] * m2->c[1][1] + m1->c[2][0] * m2->c[1][2] + m1->c[3][0] * m2->c[1][3];
  o->c[1][1] = m1->c[0][1] * m2->c[1][0] + m1->c[1][1] * m2->c[1][1] + m1->c[2][1] * m2->c[1][2] + m1->c[3][1] * m2->c[1][3];
  o->c[1][2] = m1->c[0][2] * m2->c[1][0] + m1->c[1][2] * m2->c[1][1] + m1->c[2][2] * m2->c[1][2] + m1->c[3][2] * m2->c[1][3];
  o->c[1][3] = m1->c[0][3] * m2->c[1][0] + m1->c[1][3] * m2->c[1][1] + m1->c[2][3] * m2->c[1][2] + m1->c[3][3] * m2->c[1][3];

  o->c[2][0] = m1->c[0][0] * m2->c[2][0] + m1->c[1][0] * m2->c[2][1] + m1->c[2][0] * m2->c[2][2] + m1->c[3][0] * m2->c[2][3];
  o->c[2][1] = m1->c[0][1] * m2->c[2][0] + m1->c[1][1] * m2->c[2][1] + m1->c[2][1] * m2->c[2][2] + m1->c[3][1] * m2->c[2][3];
  o->c[2][2] = m1->c[0][2] * m2->c[2][0] + m1->c[1][2] * m2->c[2][1] + m1->c[2][2] * m2->c[2][2] + m1->c[3][2] * m2->c[2][3];
  o->c[2][3] = m1->c[0][3] * m2->c[2][0] + m1->c[1][3] * m2->c[2][1] + m1->c[2][3] * m2->c[2][2] + m1->c[3][3] * m2->c[2][3];

  o->c[3][0] = m1->c[0][0] * m2->c[3][0] + m1->c[1][0] * m2->c[3][1] + m1->c[2][0] * m2->c[3][2] + m1->c[3][0] * m2->c[3][3];
  o->c[3][1] = m1->c[0][1] * m2->c[3][0] + m1->c[1][1] * m2->c[3][1] + m1->c[2][1] * m2->c[3][2] + m1->c[3][1] * m2->c[3][3];
  o->c[3][2] = m1->c[0][2] * m2->c[3][0] + m1->c[1][2] * m2->c[3][1] + m1->c[2][2] * m2->c[3][2] + m1->c[3][2] * m2->c[3][3];
  o->c[3][3] = m1->c[0][3] * m2->c[3][0] + m1->c[1][3] * m2->c[3][1] + m1->c[2][3] * m2->c[3][2] + m1->c[3][3] * m2->c[3][3];
}

/**
 * tool_gl_matrix_inv:
 * @i: location for a #ToolGlMatrix
 * @m: a #ToolGlMatrix
 *
 * Compute the inverse of @m in @i.
 *
 * Since: 3.9
 **/
void tool_gl_matrix_inv(ToolGlMatrix *i, const ToolGlMatrix *m)
{
  gfloat det = 0.f;

  det += m->c[0][0] * m->c[1][1] * m->c[2][2] * m->c[3][3];
  det += m->c[0][0] * m->c[1][2] * m->c[2][3] * m->c[3][1];
  det += m->c[0][0] * m->c[1][3] * m->c[2][1] * m->c[3][2];

  det -= m->c[0][0] * m->c[1][3] * m->c[2][2] * m->c[3][1];
  det -= m->c[0][0] * m->c[1][2] * m->c[2][1] * m->c[3][3];
  det -= m->c[0][0] * m->c[1][1] * m->c[2][3] * m->c[3][2];

  det -= m->c[0][1] * m->c[1][0] * m->c[2][2] * m->c[3][3];
  det -= m->c[0][2] * m->c[1][0] * m->c[2][3] * m->c[3][1];
  det -= m->c[0][3] * m->c[1][0] * m->c[2][1] * m->c[3][2];

  det += m->c[0][3] * m->c[1][0] * m->c[2][2] * m->c[3][1];
  det += m->c[0][2] * m->c[1][0] * m->c[2][1] * m->c[3][3];
  det += m->c[0][1] * m->c[1][0] * m->c[2][3] * m->c[3][2];

  det += m->c[0][1] * m->c[1][2] * m->c[2][0] * m->c[3][3];
  det += m->c[0][2] * m->c[1][3] * m->c[2][0] * m->c[3][1];
  det += m->c[0][3] * m->c[1][1] * m->c[2][0] * m->c[3][2];

  det -= m->c[0][3] * m->c[1][2] * m->c[2][0] * m->c[3][1];
  det -= m->c[0][2] * m->c[1][1] * m->c[2][0] * m->c[3][3];
  det -= m->c[0][1] * m->c[1][3] * m->c[2][0] * m->c[3][2];

  det -= m->c[0][1] * m->c[1][2] * m->c[2][3] * m->c[3][0];
  det -= m->c[0][2] * m->c[1][3] * m->c[2][1] * m->c[3][0];
  det -= m->c[0][3] * m->c[1][1] * m->c[2][2] * m->c[3][0];

  det += m->c[0][3] * m->c[1][2] * m->c[2][1] * m->c[3][0];
  det += m->c[0][2] * m->c[1][1] * m->c[2][3] * m->c[3][0];
  det += m->c[0][1] * m->c[1][3] * m->c[2][2] * m->c[3][0];

  g_return_if_fail(det != 0.f);

  det = 1.f / det;

  i->c[0][0]  = m->c[1][1] * m->c[2][2] * m->c[3][3];
  i->c[0][0] += m->c[1][2] * m->c[2][3] * m->c[3][1];
  i->c[0][0] += m->c[1][3] * m->c[2][1] * m->c[3][2];
  i->c[0][0] -= m->c[1][3] * m->c[2][2] * m->c[3][1];
  i->c[0][0] -= m->c[1][2] * m->c[2][1] * m->c[3][3];
  i->c[0][0] -= m->c[1][1] * m->c[2][3] * m->c[3][2];
  i->c[0][0] *= det;

  i->c[0][1] = -m->c[0][1] * m->c[2][2] * m->c[3][3];
  i->c[0][1] -= m->c[0][2] * m->c[2][3] * m->c[3][1];
  i->c[0][1] -= m->c[0][3] * m->c[2][1] * m->c[3][2];
  i->c[0][1] += m->c[0][3] * m->c[2][2] * m->c[3][1];
  i->c[0][1] += m->c[0][2] * m->c[2][1] * m->c[3][3];
  i->c[0][1] += m->c[0][1] * m->c[2][3] * m->c[3][2];
  i->c[0][1] *= det;

  i->c[0][2]  = m->c[0][1] * m->c[1][2] * m->c[3][3];
  i->c[0][2] += m->c[0][2] * m->c[1][3] * m->c[3][1];
  i->c[0][2] += m->c[0][3] * m->c[1][1] * m->c[3][2];
  i->c[0][2] -= m->c[0][3] * m->c[1][2] * m->c[3][1];
  i->c[0][2] -= m->c[0][2] * m->c[1][1] * m->c[3][3];
  i->c[0][2] -= m->c[0][1] * m->c[1][3] * m->c[3][2];
  i->c[0][2] *= det;

  i->c[0][3] = -m->c[0][1] * m->c[1][2] * m->c[2][3];
  i->c[0][3] -= m->c[0][2] * m->c[1][3] * m->c[2][1];
  i->c[0][3] -= m->c[0][3] * m->c[1][1] * m->c[2][2];
  i->c[0][3] += m->c[0][3] * m->c[1][2] * m->c[2][1];
  i->c[0][3] += m->c[0][2] * m->c[1][1] * m->c[2][3];
  i->c[0][3] += m->c[0][1] * m->c[1][3] * m->c[2][2];
  i->c[0][3] *= det;


  i->c[1][0] = -m->c[1][0] * m->c[2][2] * m->c[3][3];
  i->c[1][0] -= m->c[1][2] * m->c[2][3] * m->c[3][0];
  i->c[1][0] -= m->c[1][3] * m->c[2][0] * m->c[3][2];
  i->c[1][0] += m->c[1][3] * m->c[2][2] * m->c[3][0];
  i->c[1][0] += m->c[1][2] * m->c[2][0] * m->c[3][3];
  i->c[1][0] += m->c[1][0] * m->c[2][3] * m->c[3][2];
  i->c[1][0] *= det;

  i->c[1][1]  = m->c[0][0] * m->c[2][2] * m->c[3][3];
  i->c[1][1] += m->c[0][2] * m->c[2][3] * m->c[3][0];
  i->c[1][1] += m->c[0][3] * m->c[2][0] * m->c[3][2];
  i->c[1][1] -= m->c[0][3] * m->c[2][2] * m->c[3][0];
  i->c[1][1] -= m->c[0][2] * m->c[2][0] * m->c[3][3];
  i->c[1][1] -= m->c[0][0] * m->c[2][3] * m->c[3][2];
  i->c[1][1] *= det;

  i->c[1][2] = -m->c[0][0] * m->c[1][2] * m->c[3][3];
  i->c[1][2] -= m->c[0][2] * m->c[1][3] * m->c[3][0];
  i->c[1][2] -= m->c[0][3] * m->c[1][0] * m->c[3][2];
  i->c[1][2] += m->c[0][3] * m->c[1][2] * m->c[3][0];
  i->c[1][2] += m->c[0][2] * m->c[1][0] * m->c[3][3];
  i->c[1][2] += m->c[0][0] * m->c[1][3] * m->c[3][2];
  i->c[1][2] *= det;

  i->c[1][3]  = m->c[0][0] * m->c[1][2] * m->c[2][3];
  i->c[1][3] += m->c[0][2] * m->c[1][3] * m->c[2][0];
  i->c[1][3] += m->c[0][3] * m->c[1][0] * m->c[2][2];
  i->c[1][3] -= m->c[0][3] * m->c[1][2] * m->c[2][0];
  i->c[1][3] -= m->c[0][2] * m->c[1][0] * m->c[2][3];
  i->c[1][3] -= m->c[0][0] * m->c[1][3] * m->c[2][2];
  i->c[1][3] *= det;


  i->c[2][0]  = m->c[1][0] * m->c[2][1] * m->c[3][3];
  i->c[2][0] += m->c[1][1] * m->c[2][3] * m->c[3][0];
  i->c[2][0] += m->c[1][3] * m->c[2][0] * m->c[3][1];
  i->c[2][0] -= m->c[1][3] * m->c[2][1] * m->c[3][0];
  i->c[2][0] -= m->c[1][1] * m->c[2][0] * m->c[3][3];
  i->c[2][0] -= m->c[1][0] * m->c[2][3] * m->c[3][1];
  i->c[2][0] *= det;

  i->c[2][1] = -m->c[0][0] * m->c[2][1] * m->c[3][3];
  i->c[2][1] -= m->c[0][1] * m->c[2][3] * m->c[3][0];
  i->c[2][1] -= m->c[0][3] * m->c[2][0] * m->c[3][1];
  i->c[2][1] += m->c[0][3] * m->c[2][1] * m->c[3][0];
  i->c[2][1] += m->c[0][1] * m->c[2][0] * m->c[3][3];
  i->c[2][1] += m->c[0][0] * m->c[2][3] * m->c[3][1];
  i->c[2][1] *= det;

  i->c[2][2]  = m->c[0][0] * m->c[1][1] * m->c[3][3];
  i->c[2][2] += m->c[0][1] * m->c[1][3] * m->c[3][0];
  i->c[2][2] += m->c[0][3] * m->c[1][0] * m->c[3][1];
  i->c[2][2] -= m->c[0][3] * m->c[1][1] * m->c[3][0];
  i->c[2][2] -= m->c[0][1] * m->c[1][0] * m->c[3][3];
  i->c[2][2] -= m->c[0][0] * m->c[1][3] * m->c[3][1];
  i->c[2][2] *= det;

  i->c[2][3] = -m->c[0][0] * m->c[1][1] * m->c[2][3];
  i->c[2][3] -= m->c[0][1] * m->c[1][3] * m->c[2][0];
  i->c[2][3] -= m->c[0][3] * m->c[1][0] * m->c[2][1];
  i->c[2][3] += m->c[0][3] * m->c[1][1] * m->c[2][0];
  i->c[2][3] += m->c[0][1] * m->c[1][0] * m->c[2][3];
  i->c[2][3] += m->c[0][0] * m->c[1][3] * m->c[2][1];
  i->c[2][3] *= det;

  
  i->c[3][0] = -m->c[1][0] * m->c[2][1] * m->c[3][2];
  i->c[3][0] -= m->c[1][1] * m->c[2][2] * m->c[3][0];
  i->c[3][0] -= m->c[1][2] * m->c[2][0] * m->c[3][1];
  i->c[3][0] += m->c[1][2] * m->c[2][1] * m->c[3][0];
  i->c[3][0] += m->c[1][1] * m->c[2][0] * m->c[3][2];
  i->c[3][0] += m->c[1][0] * m->c[2][2] * m->c[3][1];
  i->c[3][0] *= det;

  i->c[3][1]  = m->c[0][0] * m->c[2][1] * m->c[3][2];
  i->c[3][1] += m->c[0][1] * m->c[2][2] * m->c[3][0];
  i->c[3][1] += m->c[0][2] * m->c[2][0] * m->c[3][1];
  i->c[3][1] -= m->c[0][2] * m->c[2][1] * m->c[3][0];
  i->c[3][1] -= m->c[0][1] * m->c[2][0] * m->c[3][2];
  i->c[3][1] -= m->c[0][0] * m->c[2][2] * m->c[3][1];
  i->c[3][1] *= det;

  i->c[3][2] = -m->c[0][0] * m->c[1][1] * m->c[3][2];
  i->c[3][2] -= m->c[0][1] * m->c[1][2] * m->c[3][0];
  i->c[3][2] -= m->c[0][2] * m->c[1][0] * m->c[3][1];
  i->c[3][2] += m->c[0][2] * m->c[1][1] * m->c[3][0];
  i->c[3][2] += m->c[0][1] * m->c[1][0] * m->c[3][2];
  i->c[3][2] += m->c[0][0] * m->c[1][2] * m->c[3][1];
  i->c[3][2] *= det;

  i->c[3][3]  = m->c[0][0] * m->c[1][1] * m->c[2][2];
  i->c[3][3] += m->c[0][1] * m->c[1][2] * m->c[2][0];
  i->c[3][3] += m->c[0][2] * m->c[1][0] * m->c[2][1];
  i->c[3][3] -= m->c[0][2] * m->c[1][1] * m->c[2][0];
  i->c[3][3] -= m->c[0][1] * m->c[1][0] * m->c[2][2];
  i->c[3][3] -= m->c[0][0] * m->c[1][2] * m->c[2][1];
  i->c[3][3] *= det;
}

/**
 * tool_gl_matrix_translate:
 * @matrix: a matrix
 * @trans: (array fixed-size=3): translation in cartesian coordinates.
 *
 * Add @trans to @matrix. So the result in multiplying @matrix to a
 * vector, will first apply @trans to the vector and then apply @matrix.
 *
 * Since: 3.9
 **/
void tool_gl_matrix_translate(ToolGlMatrix *matrix, const gfloat trans[3])
{
  ToolGlMatrix T, M;

  g_return_if_fail(matrix);

  M = *matrix;
  tool_gl_matrix_setIdentity(&T);
  T.c[3][0] += trans[0];
  T.c[3][1] += trans[1];
  T.c[3][2] += trans[2];
  tool_gl_matrix_matrixProd(matrix, &M, &T);
}

/**
 * tool_gl_matrix_rotate:
 * @matrix: a matrix
 * @angle: an angle in degrees
 * @dir: an axis.
 *
 * Modify @matrix to include a rotation along @dir of @angle.
 *
 * Since: 3.9
 **/
void tool_gl_matrix_rotate(ToolGlMatrix *matrix, gfloat angle, ToolXyzDir dir)
{
  ToolGlMatrix R, M;
  const int perm[3][3] = {{1, 2, 0}, {2, 0, 1}, {0, 1, 2}};

  g_return_if_fail(matrix);

  M = *matrix;
  tool_gl_matrix_setIdentity(&R);
  R.c[perm[dir][0]][perm[dir][0]] = cos(angle * TOOL_PI180);
  R.c[perm[dir][0]][perm[dir][1]] = sin(angle * TOOL_PI180);
  R.c[perm[dir][1]][perm[dir][0]] = -sin(angle * TOOL_PI180);
  R.c[perm[dir][1]][perm[dir][1]] = cos(angle * TOOL_PI180);
  tool_gl_matrix_matrixProd(matrix, &M, &R);
}
/**
 * tool_gl_matrix_scale:
 * @matrix: a matrix
 * @scale: (array fixed-size=3): scaling vector.
 *
 * Modify @matrix to include a scaling in the three direction given by @scale.
 *
 * Since: 3.9
 **/
void tool_gl_matrix_scale(ToolGlMatrix *matrix, const gfloat scale[3])
{
  ToolGlMatrix S, M;

  g_return_if_fail(matrix);

  M = *matrix;
  tool_gl_matrix_setIdentity(&S);
  S.c[0][0] = scale[0];
  S.c[1][1] = scale[1];
  S.c[2][2] = scale[2];
  tool_gl_matrix_matrixProd(matrix, &M, &S);
}

/**
 * ToolGlCamera:
 * @d_red: a factor for perspective from 1. to inifnity. With one, the nose of
 *         the observer is completly set on the rendered object, and the size
 *         of the observer is neglectible compared to the size of the object.
 * @theta: the theta angle in spherical coordinates of the position of the observer ;
 * @phi: the phi angle in spherical coordinates of the position of the observer ;
 * @omega: rotation of the observer on itself ;
 * @xs: a value for translation of the viewport on x axis ;
 * @ys: a value for translation of the viewport on y axis ;
 * @gross: a value of zoom ;
 * @length0: a length reference to adimension all values, by default,
 * this is the longest diagonal of the current box (without
 * duplication) ;
 * @extens: additional length to add to length0 to obtain the global
 * viewable area.
 * @up: (in) (array fixed-size=3): the current up vector.
 * @upAxis: which axis define the north pole.
 * @centre: (in) (array fixed-size=3): position of the eye look at ;
 * @eye: (in) (array fixed-size=3): position of the eye.
 * @unit: the unit of @length0 and @extens.
 * 
 * Values to define the position of the observer.
 */

static ToolGlCamera* camera_copy(ToolGlCamera *camera);

/**
 * tool_gl_camera_get_type:
 *
 * Create and retrieve a #GType for a #ToolGlCamera object.
 *
 * Since: 3.7
 *
 * Returns: a new type for #ToolGlCamera structures.
 */
GType tool_gl_camera_get_type(void)
{
  static GType g_define_type_id = 0;

  if (g_define_type_id == 0)
    g_define_type_id =
      g_boxed_type_register_static("ToolGlCamera", 
                                   (GBoxedCopyFunc)camera_copy,
                                   (GBoxedFreeFunc)g_free);
  return g_define_type_id;
}
/**
 * tool_gl_camera_copy:
 * @to: a location to copy values to
 * @from: a #ToolGlCamera to copy values from
 *
 * Do a deep copy of @from to @to.
 *
 * Since: 3.7
 **/
void tool_gl_camera_copy(ToolGlCamera *to, const ToolGlCamera *from)
{
  g_return_if_fail(from);

  if (to)
    *to = *from;
}
static ToolGlCamera* camera_copy(ToolGlCamera *camera)
{
  ToolGlCamera *out;

  out = g_malloc(sizeof(ToolGlCamera));
  tool_gl_camera_copy(out, camera);
  return out;
}

/**
 * tool_gl_camera_setThetaPhiOmega:
 * @camera: a valid #ToolGlCamera object ;
 * @valueTheta: a floatinf point value in degrees ;
 * @valuePhi: a floating point value in degrees ;
 * @valueOmega: a floating point value in degrees ;
 * @mask: to specified what values will be changed.
 *
 * Change the orientation of the camera to the specified angles.
 *
 * Returns: a mask of changed values.
 */
int tool_gl_camera_setThetaPhiOmega(ToolGlCamera *camera, float valueTheta,
                                    float valuePhi, float valueOmega, int mask)
{
  float valT, valP, valO;
  int diff;

  g_return_val_if_fail(camera, FALSE);
  
  diff = 0;
  if (mask & TOOL_GL_CAMERA_THETA)
    {
      valT = valueTheta;
      while (valT < -180.)
	valT += 360.;
      while (valT > 180.)
	valT -= 360.;

      if (camera->theta != valT)
	{
	  diff += TOOL_GL_CAMERA_THETA;
	  camera->theta = valT;
	}
    }
  if (mask & TOOL_GL_CAMERA_PHI)
    {
      valP = valuePhi;
      while (valP < -180.)
	valP += 360.;
      while (valP > 180.)
	valP -= 360.;

      if (camera->phi != valP)
	{
	  diff += TOOL_GL_CAMERA_PHI;
	  camera->phi = valP;
	}
    }
  if (mask & TOOL_GL_CAMERA_OMEGA)
    {
      valO = valueOmega;
      while (valO < -180.)
	valO += 360.;
      while (valO > 180.)
	valO -= 360.;

      if (camera->omega != valO)
	{
	  diff += TOOL_GL_CAMERA_OMEGA;
	  camera->omega = valO;
	}
    }
  return diff;
}

/**
 * tool_gl_camera_setXsYs:
 * @camera: a valid #ToolGlCamera object ;
 * @valueX: a floatinf point value in the bounding box scale
 *          (1 is the size of the bounding box) ;
 * @valueY: a floating point value in bounding box scale ;
 * @mask: to specified what values will be changed.
 *
 * Change the point where the camera is pointed to.
 *
 * Returns: a mask of changed values.
 */
int tool_gl_camera_setXsYs(ToolGlCamera *camera,
                           float valueX, float valueY, int mask)
{
  int diff;
  
  g_return_val_if_fail(camera, FALSE);
  
  diff = 0;
  if (mask & TOOL_GL_CAMERA_XS)
    {
      valueX = CLAMP(valueX, -3., 3.);

      if (camera->xs != valueX)
	{
          diff += TOOL_GL_CAMERA_XS;
	  camera->xs = valueX;
	}
    }
  if (mask & TOOL_GL_CAMERA_YS)
    {
      valueY = CLAMP(valueY, -3., 3.);

      if (camera->ys != valueY)
	{
          diff += TOOL_GL_CAMERA_YS;
	  camera->ys = valueY;
	}
    }
  return diff;
}

/**
 * tool_gl_camera_setGross:
 * @camera: a valid #ToolGlCamera object ;
 * @value: a positive floating point value.
 *
 * Change the value of the camera zoom value. If the value is higher than 10
 * it is set to 10 and if the value is negative it is set to 0.001.
 *
 * Returns: TRUE if value is actually changed.
 */
gboolean tool_gl_camera_setGross(ToolGlCamera *camera, float value)
{
  float val;
  
  g_return_val_if_fail(camera, FALSE);
  
  val = value;
  if (val < 0.02)
    val = 0.02;
  else if (val > 999.)
    val = 999.;

  if (camera->gross == val)
    return FALSE;

  camera->gross = val;
  return TRUE;
}

/**
 * tool_gl_camera_setPersp:
 * @camera: a valid #ToolGlCamera object ;
 * @value: a floating point value greater than 1.1.
 *
 * Change the value of the camera perspective value and put it in
 * bounds if needed.
 *
 * Returns: TRUE if value is actually changed.
 */
gboolean tool_gl_camera_setPersp(ToolGlCamera *camera, float value)
{
  g_return_val_if_fail(camera, FALSE);
  
  g_debug("Visu GlCamera: set persp to %g (%g).", value, camera->d_red);
  value = CLAMP(value, 1.1f, 100.f);
  if (camera->d_red == value)
    return FALSE;

  camera->d_red = value;
  return TRUE;
}

/**
 * tool_gl_camera_setUpAxis:
 * @camera: a #ToolGlCamera object.
 * @upAxis: a direction.
 *
 * In constraint observation mode, the "north" direction is a singular
 * one. Define this direction with this routine.
 *
 * Since: 3.6
 */
void tool_gl_camera_setUpAxis(ToolGlCamera *camera, ToolXyzDir upAxis)
{
  g_return_if_fail(camera);

  camera->upAxis = upAxis;
}

/**
 * tool_gl_camera_setRefLength:
 * @camera: a #ToolGlCamera object.
 * @value: a new length.
 * @unit: its measurement unit.
 *
 * Change the reference value that is used for the zoom.
 *
 * Since: 3.6
 *
 * Returns: TRUE if the value is indeed changed.
 */
gboolean tool_gl_camera_setRefLength(ToolGlCamera *camera, float value, ToolUnits unit)
{
  g_return_val_if_fail(camera, FALSE);

  g_debug("Tool GlCamera: set ref length to %g %d (prev was %g %d)",
              value, unit, camera->length0, camera->unit);

  if (camera->length0 == value && camera->unit == unit)
    return FALSE;

  camera->length0 = value;
  camera->unit    = unit;
  return TRUE;
}
/**
 * tool_gl_camera_setExtens:
 * @camera: a #ToolGlCamera object.
 * @value: a value.
 * @unit: a #ToolUnits value.
 *
 * Update the size of the object the camera is looking at. @unit
 * represents the physical unit the value @lg is giving in.
 *
 * Since: 3.9
 *
 * Returns: TRUE if the extens value is changed.
 **/
gboolean tool_gl_camera_setExtens(ToolGlCamera *camera, float value, ToolUnits unit)
{
  g_debug("Tool GlCamera: set additional length (%f) for camera %p.",
          value, (gpointer)camera);
  
  g_return_val_if_fail(camera, FALSE);

  if (camera->extens == value && camera->unit == unit)
    return FALSE;

  camera->extens = value;
  if (camera->unit != unit)
    camera->extens *= tool_physic_getUnitConversionFactor(unit, camera->unit);
  return TRUE;
}
/**
 * tool_gl_camera_getRefLength:
 * @camera: a #ToolGlCamera object.
 * @unit: a location for unit value (can be NULL).
 *
 * The zoom is define from a reference length in given unit. If @unit
 * is provided, the corresponding unit will be set.
 *
 * Since: 3.6
 *
 * Returns: the current reference length.
 */
float tool_gl_camera_getRefLength(const ToolGlCamera *camera, ToolUnits *unit)
{
  g_return_val_if_fail(camera, -1.f);

  if (unit)
    *unit = camera->unit;
  return camera->length0;
}
/**
 * tool_gl_camera_getScreenAxes:
 * @camera: a valid #ToolGlCamera.
 * @xAxis: (in) (array fixed-size=3): three float values representing x axis ;
 * @yAxis: (in) (array fixed-size=3): three float values representing y axis.
 *
 * This method is used to get the coordinates in box frame of x axis and y axis
 * of the current camera view.
 */
void tool_gl_camera_getScreenAxes(const ToolGlCamera *camera, float xAxis[3], float yAxis[3])
{
  double cth, sth, cph, sph, com, som;
  float matPhi[3][3], matTheta[3][3], matOmega[3][3];
  float matRes[3][3], matRes2[3][3];
  float axis[3];

  g_return_if_fail(camera);

  cth = cos(camera->theta * TOOL_PI180);
  sth = sin(camera->theta * TOOL_PI180);
  cph = cos(camera->phi * TOOL_PI180);
  sph = sin(camera->phi * TOOL_PI180);
  com = cos(camera->omega * TOOL_PI180);
  som = sin(camera->omega * TOOL_PI180);

  matPhi[0][0] = cph;
  matPhi[1][0] = sph;
  matPhi[2][0] = 0.;
  matPhi[0][1] = -sph;
  matPhi[1][1] = cph;
  matPhi[2][1] = 0.;
  matPhi[0][2] = 0.;
  matPhi[1][2] = 0.;
  matPhi[2][2] = 1.;

  matTheta[0][0] = cth;
  matTheta[1][0] = 0.;
  matTheta[2][0] = -sth;
  matTheta[0][1] = 0.;
  matTheta[1][1] = 1.;
  matTheta[2][1] = 0.;
  matTheta[0][2] = sth;
  matTheta[1][2] = 0.;
  matTheta[2][2] = cth;

  matOmega[0][0] = com;
  matOmega[1][0] = som;
  matOmega[2][0] = 0.;
  matOmega[0][1] = -som;
  matOmega[1][1] = com;
  matOmega[2][1] = 0.;
  matOmega[0][2] = 0.;
  matOmega[1][2] = 0.;
  matOmega[2][2] = 1.;

  tool_matrix_productMatrix(matRes, matTheta, matOmega);
  tool_matrix_productMatrix(matRes2, matPhi, matRes);

  axis[0] = 0.;
  axis[1] = 1.;
  axis[2] = 0.;
  tool_matrix_productVector(xAxis, matRes2, axis);

  axis[0] = -1.;
  axis[1] = 0.;
  axis[2] = 0.;
  tool_matrix_productVector(yAxis, matRes2, axis);
}

/**
 * tool_gl_matrix_modelize:
 * @M: (out caller-allocates): a location to store a #ToolGlMatrix.
 * @camera: a #ToolGlCamera object.
 *
 * Compute the modelview matrix associated to the given @camera. The
 * up axis and the eye position are updated by this call.
 *
 * Since: 3.9
 **/
void tool_gl_matrix_modelize(ToolGlMatrix *M, ToolGlCamera *camera)
{
  double theta_rad, d_red;
  double phi_rad; 
  double sth, cth, sph, cph, com, som;
  double distance;
  gfloat X[3], Y[3], Z[3], up[3];
  int permut[3][3] = {{1,2,0}, {2,0,1}, {0,1,2}};

  g_return_if_fail(M && camera);
 
  g_debug("Tool Camera: modelize view.");
  g_debug("Tool Camera: using ref length %g.",
          camera->length0);

  if (camera->d_red > 100.)
    d_red = 100.;
  else
    d_red = camera->d_red;
  theta_rad = camera->theta * TOOL_PI180;
  phi_rad   = camera->phi   * TOOL_PI180; 

  distance = d_red * camera->length0;

  sth = sin(theta_rad);
  cth = cos(theta_rad);      
  sph = sin(phi_rad);
  cph = cos(phi_rad);
  com = cos(camera->omega * TOOL_PI180);
  som = sin(camera->omega * TOOL_PI180);

  /* La matrice de rotation est la suivante pour passer
     des coordonnées transformées aux coordonnées de l'écran :
     /cph.cth -sph cph.sth\
     |sph.cth  cph sph.sth| (for z as north axis)
     \   -sth   0      cth/

     / cph -sph.sth sph.cth\
     |  0       cth     sth| (for y as north axis)
     \-sph -sth.cph cph.cth/
     Ainsi la caméra qui est situé en (0,0,Distance) dans le repère transformé
     devient dans le repère de l'écran : */
  camera->eye[permut[camera->upAxis][0]] = distance*sth*cph;
  camera->eye[permut[camera->upAxis][1]] = distance*sth*sph;
  camera->eye[permut[camera->upAxis][2]] = distance*cth;
   
  /* Vecteur donnant la direction verticale.
     Dans le repère transformé il est (-1,0,0). */
  camera->up[permut[camera->upAxis][0]] = -cth*cph*com + sph*som;
  camera->up[permut[camera->upAxis][1]] = -cth*sph*com - cph*som;
  camera->up[permut[camera->upAxis][2]] = sth*com;

  Z[0] = camera->centre[0] - camera->eye[0];
  Z[1] = camera->centre[1] - camera->eye[1];
  Z[2] = camera->centre[2] - camera->eye[2];
  tool_vector_nrm(Z);
  up[0] = camera->up[0];
  up[1] = camera->up[1];
  up[2] = camera->up[2];
  tool_vector_nrm(up);
  tool_vector_prod(X, Z, up);
  tool_vector_prod(Y, X, Z);

  tool_gl_matrix_setIdentity(M);
  M->c[0][0] = X[0];
  M->c[1][0] = X[1];
  M->c[2][0] = X[2];
  M->c[0][1] = Y[0];
  M->c[1][1] = Y[1];
  M->c[2][1] = Y[2];
  M->c[0][2] = -Z[0];
  M->c[1][2] = -Z[1];
  M->c[2][2] = -Z[2];
  M->c[3][0] = -X[0] * camera->eye[0] - X[1] * camera->eye[1] - X[2] * camera->eye[2];
  M->c[3][1] = -Y[0] * camera->eye[0] - Y[1] * camera->eye[1] - Y[2] * camera->eye[2];
  M->c[3][2] = Z[0] * camera->eye[0] + Z[1] * camera->eye[1] + Z[2] * camera->eye[2];
}

#undef near
#undef far
/**
 * tool_gl_matrix_project:
 * @P: (out caller-allocates): a location to store a #ToolGlMatrix.
 * @camera: a #ToolGlCamera object.
 * @width: a width.
 * @height: a height.
 *
 * Compute the projection matrix associated to the @camera. @width and
 * @height represents the size of the projection plane.
 *
 * Since: 3.9 
 **/
void tool_gl_matrix_project(ToolGlMatrix *P, const ToolGlCamera *camera, guint width, guint height)
{ 
  double x, y, xmin, xmax, ymin, ymax, fact, rap;
  double rap_win, d_red;
  double near, far, left, right, top, bottom;

  g_return_if_fail(P && camera);
  g_debug("Tool GlCamera: project view (%d).", camera->unit);
  g_debug(" | %g %g.", camera->length0, camera->extens);

  tool_gl_matrix_setIdentity(P);
  if (camera->length0 <= 0. || camera->extens < 0.)
    return;

  if (camera->d_red > 100.)
    d_red = 100.;
  else
    d_red = camera->d_red;
  
  fact = d_red * camera->length0;
  near = MAX(0.01, fact - camera->extens);
  far  = fact + camera->extens;

  if (d_red < 100.)
    {
      fact = near / camera->gross / d_red;
      rap = 2. * near / (d_red - 1.);
    }
  else
    {
      fact = camera->length0 / camera->gross;
      rap = 2. * d_red * camera->length0 / (d_red - 1.);
    }
  x = (0.5 - camera->xs) * rap;
  xmin = x - fact;
  xmax = x + fact;
  y = (0.5 - camera->ys) * rap;
  ymin = y - fact;
  ymax = y + fact;
  left   = xmin;
  bottom = ymin;
    
  rap_win = (1.0 * height) / width;
  if ( 1. > rap_win )
    {
      top   = ymax;
      fact  = (ymax - ymin) / rap_win;
      left  = 0.5 * (xmin + xmax - fact);
      right = 0.5 * (xmin + xmax + fact);
    }
  else if ( 1. < rap_win )
    {
      right  = xmax;
      fact   = (xmax - xmin) * rap_win;
      bottom = 0.5 * (ymin + ymax - fact);
      top    = 0.5 * (ymin + ymax + fact);
    }
  else
    {
      right  = xmax;
      top    = ymax;
    }

  if (camera->d_red == 100.)
    {
      /* glOrtho https://www.glprogramming.com/blue/ch05.html#id30307 */
      P->c[0][0] = 2.f / (right - left);
      P->c[1][1] = 2.f / (top - bottom);
      P->c[2][2] = -2.f / (far - near);
      P->c[3][0] = -(right + left) / (right - left);
      P->c[3][1] = -(top + bottom) / (top - bottom);
      P->c[3][2] = -(far + near) / (far - near);
    }
  else
    {
      /* glFrustum https://www.glprogramming.com/blue/ch05.html#id5478066 */
      P->c[0][0] = 2.f * near / (right - left);
      P->c[1][1] = 2.f * near / (top - bottom);
      P->c[2][0] = (right + left) / (right - left);
      P->c[2][1] = (top + bottom) / (top - bottom);
      P->c[2][2] = -(far + near) / (far - near);
      P->c[2][3] = -1.f;
      P->c[3][2] = -2.f * far * near / (far - near);
      P->c[3][3] = 0.f;
    }
}

/**
 * tool_gl_camera_getNearFar:
 * @camera: a #ToolGlCamera object.
 * @nearFar: (out caller-allocates) (array fixed-size=2): a location for two floats.
 *
 * Retrieve the value in model view for the near and far planes.
 *
 * Since: 3.9
 */
void tool_gl_camera_getNearFar(const ToolGlCamera *camera, gfloat nearFar[2])
{
  gfloat fact;

  g_return_if_fail(camera);

  nearFar[0] = 30.f;
  nearFar[1] = 20.;
  if (camera->length0 <= 0. || camera->extens < 0.)
    return;

  fact = MIN(camera->d_red, 100.) * camera->length0;
  nearFar[0] = MAX(0.01, fact - camera->extens);
  nearFar[1] = fact + camera->extens;
}
