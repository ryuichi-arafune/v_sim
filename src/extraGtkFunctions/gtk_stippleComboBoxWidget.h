/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef GTK_STIPPLECOMBOBOXWIDGET_H
#define GTK_STIPPLECOMBOBOXWIDGET_H

#include <glib.h>
#include <glib-object.h>

#include <gtk/gtk.h>

G_BEGIN_DECLS
/**
 * VISU_TYPE_UI_STIPPLE_COMBOBOX:
 *
 * Get the associated #GType to the VisuUiStippleCombobox objects.
 *
 * Since: 3.4
 */
#define VISU_TYPE_UI_STIPPLE_COMBOBOX         (visu_ui_stipple_combobox_get_type ())
/**
 * VISU_UI_STIPPLE_COMBOBOX:
 * @obj: the widget to cast.
 *
 * Cast the given object to a #VisuUiStippleCombobox object.
 *
 * Since: 3.4
 */
#define VISU_UI_STIPPLE_COMBOBOX(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_TYPE_UI_STIPPLE_COMBOBOX, VisuUiStippleCombobox))
/**
 * VISU_UI_STIPPLE_COMBOBOX_CLASS:
 * @klass: the class to cast.
 *
 * Cast the given class to a #VisuUiStippleComboboxClass object.
 *
 * Since: 3.4
 */
#define VISU_UI_STIPPLE_COMBOBOX_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), VISU_TYPE_UI_STIPPLE_COMBOBOX, VisuUiStippleComboboxClass))
/**
 * VISU_IS_UI_STIPPLE_COMBOBOX:
 * @obj: the object to test.
 *
 * Get if the given object is a valid #VisuUiStippleCombobox object.
 *
 * Since: 3.4
 */
#define VISU_IS_UI_STIPPLE_COMBOBOX(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_TYPE_UI_STIPPLE_COMBOBOX))
/**
 * VISU_IS_UI_STIPPLE_COMBOBOX_CLASS:
 * @klass: the class to test.
 *
 * Get if the given class is a valid #VisuUiStippleComboboxClass class.
 *
 * Since: 3.4
 */
#define VISU_IS_UI_STIPPLE_COMBOBOX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), VISU_TYPE_UI_STIPPLE_COMBOBOX))

typedef struct _VisuUiStippleCombobox VisuUiStippleCombobox;
typedef struct _VisuUiStippleComboboxClass VisuUiStippleComboboxClass;

GType visu_ui_stipple_combobox_get_type(void);
GtkWidget* visu_ui_stipple_combobox_new();

void visu_ui_stipple_combobox_set(VisuUiStippleCombobox *stippleComboBox, guint16 stipple);

gboolean visu_ui_stipple_combobox_setSelection(VisuUiStippleCombobox* stippleComboBox,
					       guint16 stipple);
guint16 visu_ui_stipple_combobox_getSelection(VisuUiStippleCombobox *stippleComboBox);
GdkPixbuf* visu_ui_stipple_combobox_getStamp(VisuUiStippleCombobox *stippleComboBox,
						guint16 stipple);

GdkPixbuf* visu_ui_stipple_combobox_class_buildStamp(guint16 stipple);

void visu_ui_stipple_combobox_add(VisuUiStippleCombobox *stippleComboBox, guint16 stipple);

G_END_DECLS

#endif
